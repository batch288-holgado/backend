// [Exponent Operator]
	
	// Before the ES6 Update: Math.pow(base, exponent)
	const firstNum = Math.pow(8, 2)
	console.log(firstNum)

	const secondNum = Math.pow(5, 5);
	console.log(secondNum)
	
	// After the ES6 Update: 
	const thirdNum = 8 ** 2;
	console.log(thirdNum);

	const fourthNum = 5 ** 5;
	console.log(fourthNum)

// [Section] Template Literals
	// It will allow us to write strings without using the concatenation operator
	// Greatly helps with code readability

	// Before ES6 Update
	let name = "John";
	let message = 'Hello ' + name + "! Welcome to programming!";
	console.log(message)

	// After ES6 Update: (uses backtick ``)
	message = `Hello ${name}! Welcome to programming!`
	console.log(message)
	console.log(typeof message)

	// Multi-line using Template Literals
	const anotherMessage = `${name} attended a math competition.
	He won it by solving the problem of 8**2 with the solution of ${firstNum}`;
	console.log(anotherMessage)

	const interestRate = .1;
	const principal = 1000;

	// Template literals allow us to write strings with embedded JavaScript expressions (recall: expressions are any valid unit of code that resolves to a value)
	// "${}" are used to include JavaScript expressions in strings using the template literals
	console.log(`The interest on your savings account is: ${interestRate*principal}`)

// [Section] Array Destructuring
	/*
		- ALlows us to unpack elements in an array into distinct variables
		Syntax: let/const [variableNameA, variableNameB, variableNameC, ...] = arrayName;
	*/

	// Before ES6 Update
	const fullName = ["Juan", "Dela", "Cruz"];

	let firstName = fullName[0];
	let middleName = fullName[1];
	let lastName = fullName[2];

	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

	// After ES6 Update
	const [name1, name2, name3] = fullName;
	console.log(name1)
	console.log(name2)
	console.log(name3)

	// Another example of Array Destructuring

	let gradesPerQuarter = [98, 97, 95, 94];

	console.log(gradesPerQuarter)

	let [firstGrading, secondGrading, thirdGrading, fourthGrading] = gradesPerQuarter;

	firstGrading = 100
	console.log(firstGrading);

	console.log(firstGrading);
	console.log(secondGrading);
	console.log(thirdGrading);
	console.log(fourthGrading);

// [Section] Object Destructuring 
	// Allows us to unpack properties of objects into distinct variables
	// Shortens 
	// Syntax: let/const {propertyNameA, propertyNameB, ...} = objectName;

	// Before ES6 Update
		const person = {
			givenName: "Jane",
			maidenName: "Dela",
			familyName: "Cruz"
		}

		console.log(person)

		/*const givenName = person.givenName
		const maidenName = person.maidenName
		const familyName = person.familyName

		console.log(givenName)
		console.log(maidenName)
		console.log(familyName)*/

	// After the ES6 Update

		// The order of the property/variable does not affect its value
		const {maidenName, givenName, familyName} = person
		console.log(person)

		console.log(`This is the givenName ${givenName}`)
		console.log(`This is the maidenName ${maidenName}`)
		console.log(`This is the familyName ${familyName}`)

// [Section] Arrow Function
	// Compact alternative syntax to traditional functions
	/*
		Syntax: const/let variableName = () => {
				statement/codeblock;
				}
	*/

	const printFullName = (firstName, middleInitial, lastName) => {
		console.log(`${firstName} ${middleInitial} ${lastName}`);
	}

	printFullName("John", "D", "Smith")

	// Arrow function can also be used with loops
	const students = ["John", "Jane", "Judy"];
	students.forEach((student) => {
		console.log(`${student} is a student`)
	})

// [Section] Implicit Return in an Arrow Function
	// If the function will run one line or one statement, the arrow function will implicitly return the value

	const add = (x,y) => x+y;

	let total = add(10, 12);
	console.log(total);

// [Section] Default Function Argument Value
	// Provides a default function argument value if none are provided when the function is invoked

	const greet = (name = "User", age = 78) => {
		return `Good morning, ${name}! I am ${age} years old!`;
	}

	console.log(greet("Lloyd"))

/*
	- Traditional
	function name (arguments) {
		return condition/expression
	}

	- ES6 
	let name = (arguments) => {
		return condition/expression
	}
*/


// [Section] Class-Based Object Blueprints
	// Allows us to create/instantiate objects using a class as blueprint
	/*
		Syntax: 
		class className{
			constructor(objectPropertyA, objectPropertyB){ 
				this.objectPropertyA = objectValueA;
				thisobjectPropertyB = objectValueB;
			}
		}
	*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

	// Instantiating an Object
	const myCar = new Car("Ford", "Ranger Raptor", 2021)
	console.log(myCar);