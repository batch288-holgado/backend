// console.log("Test")

// [Section] Arithmetic Operators
	let x = 1397;
	let y = 7831;

	// Addition Operator (+) 
	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	// Subtraction Operator (-)
	let difference = y - x;
	console.log("Result of subtraction opertor: " + difference);

	// Multiplication Operator (*)
	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	// Division Operator (/)
	let quotient = y / x;
	console.log("Result of division operator: " + quotient);

	// Modulo Operator (%)
	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

// [Section] Assignment Operators
	// Assignment Operator (=)
		// The assignment operator assigns or reassigns the value to the variable
		let assignmentNumber = 8;

	// Addition Assignment Operator (+=)
		// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable
		/*
			- Think: x += y is the same as x(new) = x(original) + y
			- x += y -> x(newValue) = x(originalValue) + y
			- Same applies to the other assignment operators
		*/
		
		assignmentNumber += 2;
		console.log(assignmentNumber);

		assignmentNumber += 3;
		console.log("Result of addition assignment operator: " + assignmentNumber);

	// Subtraction (-=) / Multiplication (*=) / Division Assignment (/=) Operators
		assignmentNumber -= 2;
		console.log("Result of subtraction assignment operator: " + assignmentNumber);

		assignmentNumber *= 3;
		console.log("Result of multiplication assignment operator: " + assignmentNumber);

		assignmentNumber /= 11;
		console.log("Result of division assignment operator: " + assignmentNumber);

	// Multiple Operators and Parentheses
		// MDAS = Multiplication or Division First then Addition or Subtraction, from left to right

		let mdas = 1 + 2 - 3 * 4 / 5
			/*
				1. 3 * 4 = 12
				2. 12 / 5 = 2.4
				3. 1 + 2 - 2.4
				4. 3 - 2.4 = 0.6
			*/
		console.log(mdas.toFixed(1));

		let pemdas = 1 + (2-3) * (4/5);
			/*
				1. 4/5 = 0.8
				2. 2-3 = -1
				3. 1 + (-1) * (0.8)
				4. (-1) * (0.8) = -0.8
				5. 1 - 0.8 = 0.2
			*/
		console.log(pemdas.toFixed(1))

// [Section] Increment (++) and Decrement (--)
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment or decrement is applied to.

	let z = 1;
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	increment = z++
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment: " + z);

	let n = 1;
	let decrement = --n;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + n)

	increment = z--
	console.log("Result of post-decrement: " + increment);
	console.log("Result of post-decrement: " + z);

// [Section] Type Coercion
	/*
		-Type Coercion is the automatic or implicit conversion of values from 1 data type to another. This happens when operations are performed on different data types that would normally yield possible irregular results
	*/

	let numA = '10';
	let numB = 12;
	
	let coercion = numA + numB;
	// If you are going to add a string and a number, the value will concatenate
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	// The boolean "true" is also associated with the value of 1
	let numE = true + 1;
	console.log(numE);

	// The boolean "false" is also associated with the value of 0
	let numF = false + 1;
	console.log(numF);

// [Section] Comparison Operators
let juan = 'juan';

	// Equality Operator (==)
		/*
			- This operator check whether the operands are equal or have the same content/value
			- Returns a Boolean value -> true if same, false if not
		*/

			console.log(1 == 1); // true
			console.log(1 == 2); // false
			console.log(1 == '1'); //true; this operator also attempts to convert and compare operands of different data types
			console.log(0 =='false'); //false
			console.log(0 == false); //true
			// comparing two strings that are the same
			console.log('juan' == 'juan'); // true
			console.log('juan' == 'JUAN'); // false; case-sensitive
			console.log('juan' == juan); // true

	// Inequality Operator (!=)
		/*
			- Checks whether the operands are not equal/have different content/values
			- This operator check whether the operands are equal or have the same content/value
			- Returns a Boolean value -> true if same, false if not
			Basically, if unequal sya magttrue
		*/

		console.log(1 != 1); // false
		console.log(1 != 2); // true
		console.log(1 != '1'); // false (d/t type coercion)
		console.log(0 != false); // false
		console.log('JUAN' != 'juan'); // true
		console.log(juan != 'juan'); // false

		// Strict Equality Operator (===)
			/*
				- Checks whether the operands are equal/have the same content.
				- Also compares the data types of two values.
				- In other words, wala nang type coercion
			*/

			console.log(1 === 1); // true
			console.log(1 === 2); // false
			console.log(1 === '1'); // false
			console.log(1 === true); // false
			console.log('JUAN' === 'juan'); // false
			console.log('juan' === juan); // true

		// Strict Inequality Operator (!==)
			/*
				- Checks whether the operands are not equal/have different content.
				- Also compares the data types of two values.
			*/

			console.log(1 !== 1); // false
			console.log(1 !== 2); // true
			console.log(1 !== '1'); // true
			console.log(0 !== false); //true
			console.log('JUAN' !== 'juan') // true
			console.log(juan !== 'juan') // false

// [Section] Relational Operators
	// Checks whether one value is greater or less than to the other value.

	let a = 50
	let b = 65

	// GT or Greater Than Operator (">")
	let isGreaterThan = a > b; // false
	console.log(a > b);
	console.log(isGreaterThan);

	// LT or Less Than Operator ("<")
	let isLessThan = a < b; // true
	console.log(a < b);
	console.log(isLessThan);

	// GTE or Greater Than or Equal Operator (">=")
	let isGTorEqual = a >= b; // false
	console.log(a >= b);
	console.log(isGTorEqual);

	// LTE or Less Than or Equal Operator ("<=")
	let isLTorEqual = a <= b; // true
	console.log(a <= b);
	console.log(isLTorEqual);

	// NOTE: Forced coercion also works here

	let numStr = "30"
	console.log(a > numStr); // true
	console.log(b <= numStr); // false

	let strNum = "twenty";
	console.log(b >= strNum); // false since "twenty" and 20 are different since even if  20 is coerced into a string, it is still not the same as "twenty" which is not numeric (NaN -> Not a Number)

// [Section] Logical Operators

	let isLegalAge = true;
	let isRegistered = false;

	// Logical "AND" Operator ("&&" - Double Ampersand)
		// Returns true if ALL operands are true
		let allRequirementsMet = isLegalAge && isRegistered
		console.log("Results of logical AND operator: " +allRequirementsMet); // false
	
		let EliotIs = true
		let pogi = true
		let panget = false

		let whatIsEliot = EliotIs && pogi;
		console.log("Eliot is pogi: " + whatIsEliot);

		let eliotIsNot = EliotIs && panget;
		console.log("Eliot is panget: " + eliotIsNot);

	// Logical OR Operator ("||" - Double Pipe)
		// Returns true if at least on of the operands are true
		let allRequirementsMet1 = isLegalAge || isRegistered; // true
		console.log(allRequirementsMet1);

		let anoRaw = isRegistered || panget; // false
		console.log(anoRaw);

	/*
		&& -> dapat all green flag para maging true
		|| -> kahit isa lang na green flag para maging true, kahit lahat red flag
	*/

	let a = (true && true) // true
	let b = (true && false) // false
	let c = (true || false) // true
	let d = (true && (true || false)) // true
	let d = (false || (true && false)) // false

	// Logical NOT Operator ("!" - Exclamation Point)
		// Returns the opposite value
		let someRequirementsNotMet = !isRegistered
		console.log("Result of someRequirementsNotMet: " +someRequirementsNotMet); // true, since false siya