// Conditional Statements
	// Allows us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or a separate instruction if otherwise

// [Section] if, else if and else Statement

let numA = -1

// if statement
	// Executes a statement if the specified condition is met or true

	if(numA<0){
		console.log('Hello');
	}
		/*
			Syntax:
				if(condition){
					statement;
				}
		*/

	// The result of the expression in the if's condition must result to true, or else, the statement inside the {} will not run

	console.log(numA<0)

	// Let's update the variable and run an if statement with the same condition;
	numA = 0;

	if(numA<0){
		console.log('Hello again if numA is 0!');
	} 

	// This will not run because the expression will result to a "false"
	console.log(numA<0);

	// Let's take a look at another example:
	let city = "New York";
	if (city === "New York"){
		console.log("Welcome to New York City!")
	}
	console.log(city === "New York")

// else if statement
	
	/*
		- Execute es a statement if previous conditions are false and if the specified condition is true

		- The 'else if' statement is optional and can be added to capture additional conditions to change the flow of a program

		- If the 'if' statement is true, the else if statement will no matter
	*/

	let numH = 1;
	if (numH>2){
		console.log('Hello');
	}
	else if (numH<2){
		console.log('World');
	}

	// We ran the else if() statement after we evaluated that the if condition was false

	numH = 2

	if(numH  === 2){
		console.log('Hello');
	}
	else if(numH > 1){
		console.log('World');
	}

	// else if() statement was no longer run because the if statement ran, the evaluation of the whole statement stops there

	city = "Tokyo";
	if(city === "New York"){
		console.log("Welcome to New York City!");
	}
	else if (city === "Manila"){
		console.log("Welcome to Manila City, PH!");
	}
	else if(city === "Tokyo"){
		console.log("Welcome to Tokyo, Japan");
	}

	// Since we failed the condition for the if() and the first else if(), we went to the second else if() and checked

// else statement
	
	/*
		- Executes a statement if all other above conditions are false.
		- The "else" statement is optional and can be added to capture any other possible result to change the flow of a program.
	*/

	let numB = 0
	if(numB>0){
		console.log("Hello from numB!");
	}
	else if(numB<0){
		console.log("World from B!");
	}
	else{
		console.log("Again from numB")
	}

	/*
		- Since the preceeding if and else if conditions failed, the else statement was run isntead.

		- Note the "else if" and "else" statements will only run if  there is an "if" statement

		- "else" will still work even if there is no "else if" statement

		ERROR!
		else{
			console.log("Will not run without an if!")
		}

		else if(numB === 2){
			console.log("This will cause an error!");
		}
	*/

// if, else if, and else Statements with functions
	// Most of the time, we would like to use if, else if, and else statements with functions to control the flow of our program.

	// Example: Creating a function that will tell the typhoon intensity by providng the wind speed.

	function determineTyphoonIntensity(windSpeed){
		
		if(windSpeed < 0){
			return "Invalid wind speed";
		}
		
		else if(windSpeed <= 38 && windSpeed >= 0){
			return "Tropical Depression Detected!";
		}
		// Recall: && - both statements must be true; || - only 1 statement has to be true

		else if(windSpeed >= 39 && windSpeed <=73){
			return "Tropical Storm Detected!";
		}

		else if(windSpeed >= 74 && windSpeed <= 95){
			return "Signal #1 Detected!";
		}

		else if(windSpeed >= 96 && windSpeed <= 110){
			return "Signal #2 Detected!";
		}

		else if(windSpeed >= 111 && windSpeed <= 129){
			return "Signal #3 Detected!";
		}

		else if(windSpeed >= 130 && windSpeed <= 156){
			return "Signal #4 Detected!";
		}

		else{
			return "Signal #5 Detected!";
		}

	}
	console.log(determineTyphoonIntensity(30));
	console.log(determineTyphoonIntensity(-1));
	console.log(determineTyphoonIntensity(157));

	// console.warn() is a good way to print warning in our console that could help us developers act on a certain output within our code.
	console.warn(determineTyphoonIntensity(40));

// [Section] Truthy and Falsy
	
	/*
		- In JavaScript, a truthy value is a value that is considered true when encountered in a boolean context

		- Falsy values/exemption for truthy (remember: "f00nuN"):
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. NaN

		- Basically, entering the falsy values will not show anything
	*/

	if(true){
		console.log("Truthy");
	}

	if(1){
		console.log("Truthy")
	}

	if(false){
		console.log("Falsy")
	}

	if(0){
		console.log("Falsy")
	}	

// [Section] Conditional (Ternary) Operator
	/*
		- The Ternary Operator takes in 3 operands:
			1. Condition
			2. Expression to execute if the condition is truthy
			3. Expression to execute if the condition is falsy

		- It can be used to an if else statement
		- Ternary Operators have an implicit return statement, meaning without "return", the resulting expression can be stored in a variable.

		- Syntax:
			condition ? ifTrue : ifFalse
	*/

	let ternaryResult = (1 < 18) ? true : false;
	console.log("Result of Ternay Operator: " + ternaryResult)

	let exampleTernary = (0) ? "The numebr is not equal to zero" : "The number is equal to zero";
	console.log(exampleTernary);
	// this will return the ifFalse statement because 0 is a falsy

	// Multiple Statement Execution

	function isOfLegalAge(){
		let name = "John"
		return "You are in the legal age limit, " + name
	}

	function isUnderAge(){
		let name = "John"
		return "You are under the age limit, " + name
	}

	/*let age = parseInt(prompt("What is your age?"));
	console.log(age);
	console.log(typeof age);*/

	/*let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge()
	console.log(legalAge);*/

	// The paresInt function converts the input received from a string data type into a number

// [Section] Switch Statement

	/*
		- Switch statement evaluates an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.

		- The break statement is used to terminate the current loop once the match has been found

		Syntax:
		switch (expression/variable){
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
	*/

	// the ".toLowerCase()" function will change the input received from the prompt into all lowercase letters/
	
	

	/*let day = prompt("What is the day today?").toLowerCase();

	switch(day){
		
		case "monday": 
			console.log("The color of the day is red!");
			break;

		case "tuesday":
			console.log("The color of the day is orange!");
			break;

		case "wednesday":
			console.log("The color of the day is yellow!");
			break;

		case "thursday":
			console.log("The color of the day is green!");
			break;

		case "friday":
			console.log("The color of the day is blue!");
			break;

		case "saturday":
			console.log("The color of the day is indigo!");
			break;

		case "sunday":
			console.log("The color of the day is violet!");
			break;

		default:
			console.log("Please input a valid day!")
	}*/

	

// [Section] Try - Catch - Finally Statement
	/*
		- Try - Catch statements are commonly used for error handling
		- They are instances when the application returns an error/warning that is not necessarily an error in the context of our code.
	*/

	function showIntensityAlert(windSpeed){
		try{
			alert(determineTyphoonIntensity(windSpeed));
		}

		// The catch will only run iff there is an error statement inside our try
		catch(error){
			console.warn(error.message);
			console.log(typeof error)
		}

		finally{
			// continues execution of code regardless of success or failure; if no error kay try, diretso kay finally
			alert('Intensity updates will show new alert');
		}
	}

	showIntensityAlert(50);