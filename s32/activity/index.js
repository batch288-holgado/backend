//Add code here

let http = require('http');
let app = http.createServer(function(request, response){

    // GET Method
    if (request.url == '/' && request.method == "GET"){

        response.writeHead(200, {'Content-type' : "text/plain"})
        response.end('Welcome to booking system')

    }

    if (request.url == '/profile' && request.method == "GET"){

        response.writeHead(200, {'Content-type' : "text/plain"})
        response.end('Welcome to your profile')

    }

    if (request.url == '/courses' && request.method == "GET"){

        response.writeHead(200, {'Content-type' : "text/plain"})
        response.end('Here are our available courses')

    }

    // POST Method
    if (request.url == '/addCourse' && request.method == "POST"){

        response.writeHead(200, {'Content-type' : "text/plain"})
        response.end('Add courses to our resources')

    }

    // PUT Method
    if (request.url == '/updateCourse' && request.method == "PUT"){

        response.writeHead(200, {'Content-type' : "text/plain"})
        response.end('Update a course to our resources')

    }

    // DELETE Method
    if (request.url == '/archiveCourse' && request.method == "DELETE"){

        response.writeHead(200, {'Content-type' : "text/plain"})
        response.end('Archive courses to our resources')

    }

})

console.log('Server is running at localhost 4000')


//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
