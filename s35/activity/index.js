const express = require ("express");
const mongoose = require("mongoose");
	// mongoose is a package that allows creation of schemas to model our data structures
	// it also has access to a number of methods for manipulating our database

const port = 3001;
const app = express()

// Middlewares
	
	// Allows our app to read JSON data
	app.use(express.json())

	// Allows our app to read data from forms
	app.use(express.urlencoded({extended:true}))


// Section: MongoDB Connection
/*
	- Connecting to the database by passing the connection string
	- Due to update in MongoDB drivers that allow connection, the default connection string is being flagged as an error/warning
	- By default, a warning will be displayed in the terminal when the application is running 
	- {newUrlParser:true}
	Syntax:
		mongoose.connect("MongoDB string", {useNewUrlParser:true})
*/
	
mongoose.connect("mongodb+srv://admin:admin@batch288holgado.3ynebd7.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser:true});

//	Notification whether we connected properly with the databse
let db = mongoose.connection;
	
	// For catching the error in the case of an error during the connection; console.error.bind allows us to print errors in the browser and in the terminal
	db.on("error", console.error.bind(
		console, "Error! Can't connect to the Database"))

	// If the connection is successful:
	db.once("open", () => console.log("We're connected to the cloud database!"));

// [Section] Mongoose Schemas
/*
	- Schemas determine the structure of the document to be written in the database

	- Schemas act as blueprints to our data 

	- Use the Schema() constructor of the mongoose module to create a new Schema object
*/

	const taskSchema = new mongoose.Schema({
	// taskSchema is user-defined; ideally, it is best to name it: collectionNameSchema
		
		// Define the fields with the corresponding data types
		name: String,

		// Adding another field
		status: {
			type: String, 
			default: "pending"
		}

	})

// [Section] Models
/*
	- Models use schemas and are used to create/instantiate objects that correspond to the schema

	- Models act as the middleman from the server (JS code) to our database

	- To create a model, we are going to use the model();
*/

const Task = mongoose.model('Task', taskSchema);
/*
	- The variable "const variableName" will be the variable name we will constantly refer to here in our codebase (e.g. variableName.find.())

	- the 'Task' in mongoose.model('Task', taskSchema) will be the name that the collection will have in the database (mongoDB), where it will automatically convert into a plural form and also in lowercase

	- the 'taskSchema' in mongoose.model('Task', taskSchema) is the variable that we made above, where we instantiated with new mongoose.Schema()
*/

// [Section] Routes
/*
	Creating a POST route to create a new task
		- Create a new task
		- Business Logic

			1. Add a functionality to check whether there are duplicate tasks
				-- If the task is existing in the database, we return an error
				-- If the task does not exist in the database, we add it in the database

			2. The task data will be coming from the request's body
*/

app.post("/tasks", (request, response) => {

	// findOne method is a mongoose method that acts similarly to db.name.find() in MongoDB

	// if the findOne finds a document that matches the criteria, it will return the object/document; if there is no object that matches the criteria, it will return an empty object or null
	console.log(request.body.name)
	Task.findOne({name: request.body.name})
	.then(result => {

		// we can use the if statement to check or verify whether we have an object found

		if (result !== null) {
			return response.send("Duplicate task found!");
		}else{
			// Creating a new task which will be saved in the database
			let newTask = new Task({
				name: request.body.name
			})
		
			// the save() method will store the information to the database; since the newTask was created from the Mongoose Schema and Task Model, it will be saved in Tasks collection
			newTask.save()

			return response.send('New task created!')
		}

	})
})

	// GET all tasks in our collection
	/*
		1. Retrieve all the documents
		2. If an error is encountered, print the error
		3. If no error/s is/are found, send a success status to the client and show the documents retrieved
	*/

	app.get("/tasks", (request, response) => {

		// the find method is a mongoose method that is similar to MongoDB find
		Task.find({}) // this one is like fetch
		
		.then(result => {
			return response.send(result);
		})

		.catch(error => response.send(error))

	})

// ACTIVITY SECTION
const userSchema = new mongoose.Schema({
		username: String,
		password: String
	})

const User = mongoose.model('User', userSchema)

app.post("/signup", (request, response) => {

	console.log(request.body.username)
	User.findOne({username: request.body.username})
	.then(result => {

		if (result !== null) {
			return response.send("Duplicate username found")
		}else{

			if (request.body.username && request.body.password !== null){
				let newUser = new User({
					username: request.body.username,
					password: request.body.password
				})

				newUser.save()
				return response.send("New user registered!")
			}else{
				return response.send('BOTH username and password must be provided')
			}

		}

	})

})

app.get("/signup", (request, response) =>{
	User.find({})

	.then(result => {
		return response.send(result);
	})

	.catch(error => response.send(error))
})

if(require.main === module){
	app.listen(port, () => {
		console.log(`Server is running at port ${port}!`)
	})
}

module.exports = app;