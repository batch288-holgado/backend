// _MONGOSH
/*
	- “show databases” -> to see the list of databases
	- "use dbName'" -> to use a specific db
	- “show collections” -> to see the list of all the collections inside the db
	- "help" to see all the possible comands
	- "cls" - to clear
*/

// CRUD Operation
/*
	- CRUD Operation is the heart of any backend application
	- Mastering the CRUD operation is essential for any developer, especially for those who want to become backend developers
*/

// [Section] Inserting Documents (Create Operation)
	// Insert One Document
		/*
			- Since MongoDB deals with objects as its structure for documents, we can easily create them by providing objects in our method/operation.
			- TIP: Type it here first then copy paste to mongosh, kasi pag error na dito, sure na error na rin sa _mongosh


			Syntax: 
				db.collectionName.insertOne({
					object
				})
		*/
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

	// Insert Many
	/*
		- Instead of just 1 collection, this allows many dollections; store them in an array
		
		Syntax:
			db.collectionName.insertMany([
				{objectA}, 
				{objectB}
			])
	*/

db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		},

		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contac: {
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}
	])

// [Section] Finding Documents (Read Operation)
	// db.collectionName.find();
		db.users.find();
		// Using the find() method will show you the list of all the documents inside the collection

	// db.collectionName.find({fieldValue});
		db.users.find({firstName : "Stephen"})	
			// Using this will return the documents that will pass the criteria given in the method
		
		db.users.find({_id : ObjectId("646c5d076660eae690409ee0")}) 
			// use this syntax if fieldValue is the objectID

	// Using multiple criteria
		db.users.find({lastName: "Armstrong", age: 82})

// [Section] Updating Documents (Update Operation)
db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact:{
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});
	//updateOne method
		/*
			Syntax:
				db.collectionName.updateOne(
					{criteria},
					{
						$set: {
							field:value
						}
					}
				)
		*/
		db.users.updateOne(
			{firstName: "test"},
			{
				$set: {
					firstName:"Bill",
					lastName:"Gates",
					age:65,
					contact: {
						phone: "12345678",
						email: "bill@gmail.com"
					},
					courses: ["PHP", "Laravel", "HTML"],
					department: "Operations",
					status: "none"
				} // you can choose specific items you want to edit, it doesn't have to be all of them
			}
		)

			/*	
			db.users.updateOne(
				{firstName: "Bill"},
				{
					$set: {
						firstName: "Eliot"
					}
				}
			)
			*/


		db.users.updateOne(
			{firstName:"Jane"},
			{
				$set: {
					firstName:"Edited"
				}
			}
		) // this will update only the first document with firstName:"Jane"

	// Updating Multiple Documents
		/*
			Syntax:
				db.collectionName.updateMany(
					{criteria},
					{
						$set{
							field:value
						}
					}
				)
		*/

		db.users.updateMany(
			{department: "none"},
			{
				$set: {
					department: "HR"
				}
			}
		)

	// Replace One
		/*
			Syntax:
				db.collectionName.replaceOne(
					{criteria},
					{
						$set: {
							field:value
						}
					}
				)
		*/

		db.users.replaceOne(
			{firstName: "test"},
			{
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact: {},
				courses: [],
				department: "Operations"
			}
		)

// [Section] Deleting Documents (Delete Operation)
	// Deleting Single Document
	/*
		Syntax:
			db.collectionName.deleteOne({criteria})
	*/
		db.users.deleteOne({firstName:"Bill", contact:{}}) // if there are multiple that meet the criteria, only the first document will be deleted

	// Deleting Multiple Documents
	/*
		Syntax:
			db.collectionName.deleteMany({criteria})
	*/
		db.users.deleteMany({firstName:"Jane"})
	// NOTE!! 
		db.users.deleteMany({}); //this will delete ALL the documents in the database


		db.users.updateOne(
			{firstName: "Edited"},
			{
				$set: {
					firstName: "Jane"
				}
			}
		)