// Single Line Comment

/*
	Multi Line Comment (ctrl+shift+/)

*/

// [Section] Syntax, Statements and Comments

	// Statements in programming are instructions that we tell the computer to perform

	// JavaScript Statements usually end with a semicolon (;)

	// Semicolons are NOT required in JS, but we will use it to help us prepare for the other stric languages like Java where it is required

	// A syntax in programming is the set of rules that describe how statements must be constructed

	// All lines/blocks of code should be written in a specific manner, or else, the statement will not run

// [Section] Variables

	// Variables are used to contain/store data

	// Any information that is used by an application is stored in what we call memory

	// When we create variables, certain portions of the device's memory is given a name, which we call "variables"

	// Declaring Variables

		// Declaring variables tells our devices that a certain variable's name is created and is ready to store data

		// Syntax:
			// let/const variableName

let myVariable;

	// by default, if you declare a variable without initializing its value, it will become "undefined"

	// console.log() is useful for printing values of a variable or a certain result of code into the Google Chrome Browser's console (inspect -> console)
console.log(myVariable);

	/*
		Guides in writing variables:
			1. Use the "let" keyword, followed by the variable name of your choice and use the assignment operator (=) to assign a value.
			2.  Variable names should start with a lowercase character; use a cameCase for multiple words.
			3. For constant variables, use the "const" keyword.
			4. Variable names should be indicative (descriptive) of the value being stored to avoid confusion.
	*/

// Declaring and Initializeing Variables

// Initializing variables -> the instance when a variable is given its initial or starting value

	// Syntax
		// let/const varaibleName = value;

	// Examples
let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// In the context of certain applications, some variables/information are constant and should not change.
	// Example: The interest rate for a loan or savings account or a mortgage must not change due to real world concerns

const interest = 3.539;


// Reassigning Variable Values
	// Rassigning a variable means changing its initial or previous value into another value
	// Syntax: variableName = newValue
	// Example

productName = 'Laptop'
console.log(productName);

	// The value of a variable declared using the const keywords cannot be reassigned

/*interest = 4.489
console.log(interest);*/

// Reassigning Variables vs. Initializing Variable

	// Initializing and Declaring
let supplier = "John Smith Tradings";
console.log(supplier)

	// Reassigning
supplier = "Uy's Trading";
console.log(supplier)

	// Both
let consumer = "Chris";
consumer = "Topher";
console.log(consumer)

// Const variables cannot be declared without Initialization
	
	// Working
const driver = "Warlon Jay"

	/*Error
const driver
driver = "Warlon Jay"*/

// var vs let/const keyword
	// var is also used to declare variables, however it is an EcmaScript 1 version (1997) -> we will NOT USE var!!
	// "let" and "const" keywords were introduced as new features in ES6 (2015)

// let/const vs var
	// There are issues associated with variables declared/created using var, issues regarding hoisting
	// Hoisting is JavaScript's default behavior of moving declarations to the top
	// In terms of variables and constants, keyword "var" allows hoisting, whereas let/const do not

	// Example of Hoisting
		
		// No error
a = 5;
console.log(a);
var a;

		/*Error
b = 6;
console.log(b)
let b*/

// let/const local/global scope
	
	// Scope essentially means where the variables will be available to accessible for use

	// let and const are block scoped
	// Block: Chunk of code bounded by {}. A block lives inside curly braces. Anything within the curly braces are blocks

let outerVariable = "hello";

	/* Error

	{
		let innerVariable = "hello again";
	}
	console.log(innerVariable)

	*/

	// No Error

let globalVariable
	{
		let innerVariable = "hello again";
		console.log(innerVariable)
		console.log(outerVariable)

		globalVariable = "innerVariable"
	}
console.log(globalVariable)

// Multiple Variable Declarations and Initialization
	// Multiple variables may be declared in one statement

let productCode = "DC017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

	// Multiple variables to be consoled in one line.
console.log(productCode, productBrand);

// Using a variable with a reserved keyword
	/* Error; reserved keywords cannot be used as variable names, since they already have their own respective functions in JavaScript.
	
	let let = "Hi I'm let keyword"
	console.log(let)
	
	*/

// [Section] Data Types

	// Strings 
		// Strings are series of characters that create a word, phrase, sentence, or anything relating to creating texts.
		// String in JavaScript can be written using either a single quote ('') or double quote ("")

	let country = 'Philippines';
	let province = "Metro Manila"
	console.log(country, province)

		// Concatenating of Strings in JavaScript
			// Multiple string values can be combined to create a single string using the "+" symbol

		let fullAddress = province + ", " + country;
		console.log(fullAddress);

		let greeting = 'I live in the ' + country
		console.log(greeting);

	// Escape Characters
		
		// "\n\" will put the following words into the next line; without this, the code will ERROR. This escape character in strings in combinatino with other characters can produce different effects/results. \n\n -> 2 lines
		let mailAddress = 'Metro Manila \n\ Philippines'
		console.log(mailAddress)

		// "\'" will allow the use of an apostrophe inside a single quote; otherwise, the code will error
		let message = "John's employees went home early.";
		console.log(message)

			/* Error 
			message = 'John's employees went home early.';
			*/

			// No Error
			message = 'John\'s employees went home early.';
			console.log(message);

	// Numbers
		// Integers/Whole Numbers
		let headcount = 26;
		console.log(headcount)

		// Decimal Number/Fractions
		let grade = 98.7;
		console.log(grade);

		// Exponential Notation
		let planetDistance = 2e10;
		console.log(planetDistance);

		// Concatenating Numbers
		let headDistance = headcount + planetDistance;
		console.log(headDistance);

		// Combining text and strings (= string data type)
		console.log("John's grade last quarter is" + " " + grade);

	// Boolean
		// These values are normally used to create values relating to the state of certaint things
		let isMarried = false;
		let isGoodConduct = true;
		console.log("isMarried: " +isMarried);
		console.log("isGoodConduct " +isGoodConduct);

	// Arrays
		// These are special kinds of data used to store multiple related values
		// In JS, Arrays can store different data type, but are normally used to store SIMILAR data types

		// Similar Data Types
			// Syntax: let/const arrayName = [elementA, elementB, elementC,...]

		let grades = [98.7, 92.1, 90.2, 94.6];
		console.log(grades);

		// Different Data Types -> NOT RECOMMENDED, NOT BEST PRACTICE!
		let details = ["John", "Smith", 32, true];
		console.log(details);

	// Objects
		// These are other special kinds of data types used to mimic real world objects/items
		/* 
			
			Syntax:
			let/const objectName = {
				propertyA: valueA
				propertyB: valueB
			}

		*/

		let person = {
			fullName: "Juan Dela Cruz",
			age: 35,
			isMarried: false,
			contact: ["+63 917 123 456", "+63 917 654 321"],
			address: {
				houseNumber: '345',
				city: 'Manila'
			}
		};
		console.log(person);
			// Don't forget the commas, kung wala yan, mageerror lang

		// "typeof" operator is used to determine th etype of data or value of a variable.
		console.log(typeof mailAddress);
		console.log(typeof headcount);
		console.log(typeof isMarried);

		console.log(typeof grades);

		// Note: Array is a special type of object with methods and functions to manipulate

	// Constant Objects and Arrays
		/*
		The keyword "const" may be a little misleading

		It does not define a constant value, it defines a constant REFERENCE to a value

		Because of this, you cannot reassign a constant value/array/object

		but you can change the elements of a constant array and the properties of a constant object
		*/

		/* ERROR
			
			const anime = ["One Piece", "One Punch Man", "Attack on Titan"];

			anime = ["One Piece", "One Punch Man", "Kimetsu no Yaiba"]

		*/		

		const anime = ["One Piece", "One Punch Man", "Attack on Titan"];
		anime[2] = "Kimetsu no Yaiba"
		console.log(anime)

	// Null
		// This is used to INTENTIONALLY express the absence of a value in a variable -> you initialized but there is not value

		let spouse = null;
		spouse = "Maria"; 
		// this is reassigning

	// Undefined
		// This represents the state of a variable that has been declared, but without an assigned value -> you don't initialize at first

		let fullname;
		fullname = "Maria"
		// this is initializing

	console.log(spouse)
	console.log (fullname)