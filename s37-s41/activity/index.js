// Creating server via express
const express = require('express')
const port = 4001
const app = express()
const usersRoutes = require('./routes/usersRoutes.js')
const coursesRoutes = require('./routes/coursesRoutes.js')

/*Using CORS (Cross Origin Resource Sharing)
	this allows our backend application to be available for the frontened application; it also allows us to control the app's Cross Origin Resource Sharing*/
const cors = require('cors')

// Connection to MongoDB
const mongoose = require('mongoose');
mongoose.connect("mongodb+srv://admin:admin@batch288holgado.3ynebd7.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {
		usenewUrlParser: true,
		useUnifiedTopology: true
	})

const db = mongoose.connection
	db.on("error", console.error.bind(console, "Error, can't connect to the db!"))

	db.once("open", () => console.log("We are now connected to the db!"))

// Middlewares
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use("/users", usersRoutes)
app.use("/courses", coursesRoutes)

if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
	    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
	});
}

module.exports = {app,mongoose};