const express = require ('express')
const usersControllers = require ('../controllers/usersControllers')
const router = express.Router()
const auth = require('../auth.js')

// Routes
router.post("/register", usersControllers.registerUser)
router.post("/login", usersControllers.loginUser)
router.get("/details", auth.verify, usersControllers.getProfile)
// if the auth.verify function here is met, it will direct the response to usersControllers.getProfile -> enter token in authorization (bearer token) -> enter _id for getProfile (otherwise, it will show that catch.error)

router.get('/userDetails', auth.verify, usersControllers.retrieveUserDetails)

// Route for tokenVerification
// router.get("/verify", auth.verify)

// Route for course enrollment
router.post('/enroll', auth.verify, usersControllers.enrollCourse)

module.exports = router;