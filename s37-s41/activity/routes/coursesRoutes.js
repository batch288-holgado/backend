const express = require('express')
const coursesControllers = require('../controllers/coursesControllers')
const router = express.Router()
const auth = require('../auth.js')

//Routes
router.post('/addCourse', auth.verify, coursesControllers.addCourse)

module.exports = router;

// Route for retrieving all courses
router.get('/', auth.verify, coursesControllers.getAllCourses)

// Route for getting all active courses
router.get('/getActiveCourses', coursesControllers.getActiveCourses)

// Router for inactive courses 
router.get('/inactiveCourses', auth.verify, coursesControllers.getInactiveCourses)

// Route for getting the specific course information
router.get('/:courseId', coursesControllers.getCourse)

// Route for updating courses
router.patch('/:courseId', auth.verify, coursesControllers.updateCourse)

// Router for archiving courses
router.patch('/:courseId/archiveCourse', auth.verify, coursesControllers.archiveCourse)

// !! NOTE: GROUP ALL THE ROUTERS WITH PARAMS TOGETHER AT THE BOTTOM. IF YOU PUT A ROUTER WITHOUT A PARAM BELOW THEM, IT WILL CAUSE AN ERROR!!!!!!!!!