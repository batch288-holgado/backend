const jwt = require("jsonwebtoken");
// this one allows us to use the jsonwebtoker we have installed

const secret = "CourseBookingAPI"
// this is a user-defined string data that will be used to create our JSON web token which will be used in the algorithm for encrypting our data; this makes it difficult to decode the information without the defined keyword

// basically, this will be our phrase which will serve as the key/ticket/token

// [Section] JSON Web Token Creation
	// The JWT is a way of securely passing information from the server to the frontend or to the parts of the server

// Function for token creation
	// Analogy: We are going to load a chest and provide a lock with the "secret" code as the key
	
	// The argument that will be passed in the parameter will be the document or object that contains the info of the user

module.exports.createAccessToken = (user) => {
// this function will be invoked in the controller it will be assigned to; the parameter involved will contain the document to be retrieved

	const data = {
		id : user._id,
		isAdmin: user.isAdmin,
		email: user.email
	}

	// Generation of a JWT using JWT's sign() method;
	// Generation of the token using the "data" variable and the "secret" code with no additional options provided (in the {})
	return jwt.sign(data, secret, {});

}

// [Section] JSON Web Token Verification
/*
	Analogy: 
		Receiving the chest and opening the lock to verify if the sender is legitimate and the gift was not tampered with
*/
module.exports.verify = (request, response, next) => {
	// the "next" parameter here means that if the condition is met, the response will be directed to the next function (see usersRoutes.js ("/details"))

	let token = request.headers.authorization;
	// in postman -> authorization -> type = bearer token -> paste auth value 

	// console.log(token)
	// this will show "Bearer (token)" in the terminal, so in order to isolate the (token), we have to remove "Bearer "

	if (token !== undefined) {
		token = token.slice(7, token.length)
		// recall that in using the slice method -> slice(startingIndex, endingIndex) -> this will remove all indeces not within this

		// Validate the token using the "verify" method in decrypting the token with the secret code
		return jwt.verify(token, secret, (error, data) => {
			// the verify() method requires the token from the frontend (request.headers.authorization) and the key ("secret" as established in createAccessToken), followed by a function with 2 user-defined parameters: 1st parameter will capture the error, 2nd parameter will capture the data

			if(error){
				return response.send(false)
				// this will be the response if the token you provide is not associated with any document in the database

			}else{
				// return response.send("The token is good!")
				// this will be the response if the token provided matches with a document in the database

				next();

			}
		})

	}else{
		return response.send(false)
	}

}

// Decoding the Encrypted Token
module.exports.decode = (token) => {
	token = token.slice(7, token.length);
	// token.slice to remove the "Bearer "
	
	return jwt.decode(token, {complete: true}).payload
	// The decode() method is used to obtain the information from the JWT; the {complete: true} option allows us to return additional information from the JWT
	// JWT has 3 properties: header, payload, secret; using .payload accesses all the contents in payload
}