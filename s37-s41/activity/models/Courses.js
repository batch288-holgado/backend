const mongoose = require ('mongoose')
const courseSchema = new mongoose.Schema ({
	// READ: we make a schema named "courseSchema" using the method mongoose.schema({})

	name: {
		type: String,
		required: [true, "Course name is required"] // this option makes this field REQUIRED; by default, this is 'false'
	},

	description: {
		type: String,
		requried: [true, "Course description is required"]
	},

	price: {
		type: Number,
		required: [true, "Course price is required"]
	},

	isActive: {
		type: Boolean,
		required: [true, "Course status is required"]
	},

	createdOn: {
		type: Date,
		default: new Date() // this expression instantiates a new date that stores the current date and time whenever a course is created
	},

	slots: {
		type: Number,
		required: [true, "Course slots is required"]
	},

	enrollees: [
		{
			userID: {
				type: String,
				required: [true, "User ID of the enrollee is required!"]
			},

			enrolledOn:{
				type: Date,
				default: new Date()
			}
		}
	]

})

// Creating a model
const Courses = mongoose.model("Course", courseSchema)
	// READ: we make a variable name "Courses" and we use the mongoose.model() method to make it a model named "Course" from the schema named "courseSchema"

module.exports = Courses;
	// READ: we make the model named "Courses" exportable