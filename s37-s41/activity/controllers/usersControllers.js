const Users = require('../models/Users.js')
const Courses = require('../models/Courses.js')
const bcrypt = require('bcrypt')
// install bcrypt through npm install bcrypt in the terminal; use this code to use the bcrypt module

const auth = require("../auth.js")

// Controllers
	
// Controller for the signup
	
/*
	registerUser

	Business Logic/Flow:
		1. First we have to validate whether the user is existing or not. We can do this by validating whether the email exists in our database or not.

		2. If the user email is existing, we will prompt an error, telling the user that the email is already taken.

		3. Otherwise, we will sign up or add the user in our database.
*/
	
module.exports.registerUser = (request, response) => {

	Users.findOne({email: request.body.email})
	.then(result => {
		if (result !== null){
			return response.send(false)
		}else{
			let newUser = new Users ({ 
			// REMEMBER that the Users here is from the model Users

				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10), 

				/*
					hashSync method: it encrypts the request.body.password

					10 is the salt rounds
				*/


				isAdmin: request.body.isAdmin,
				mobileNo: request.body.mobileNo

			})
			
			// save the user
			newUser.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false))

		}
	}).catch(error => response.send(false))

}

// Controller for the authentication of the user
module.exports.loginUser = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
		if (!result){
			// !result means that result is NOT existing; since by default, if there is no condition, it means 'if result = true'

			return response.send(false)

		}else{

			// the bcrypt.compareSync method is used to compare a non-encrypted password from the login form to the encrypted password retrieved from the find method; it will return a TRUE or a FALSE, depending on the result of the comparison
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if (isPasswordCorrect){
				// by default, this means: isPasswordCorrect = true

				return response.send(
					{auth: auth.createAccessToken(result)}
					// this is the function with the (data) parameter in auth.js file; it will return a token since that is what the function (in auth.js) invokes
				)

			}else{
				return response.send(false)
			}

		}
	}).catch(error => response.send(false));
}

module.exports.getProfile = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	// this one decodes the token which is an input in request.headers.authorization; since there is a .payload in the decode function (in auth.js), it will console the payload (id, isAdmin, email, iat)

	if(userData.isAdmin){
		return Users.findOne({_id: request.body._id})
		/*
			if you will use findById, you can simply input:
				Users.findById(request.body._id)
		*/

		.then(result => {
			result.password = ""
			response.send(result)
			}).catch(error => response.send(false));
		
	}else{
		return response.send(false)
	}

}

// Controller for the enroll course
module.exports.enrollCourse = (request, response) => {

	const courseId = request.body.id 
	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		return response.send(false)
	}else{

		// Push to user document
		let isUserUpdated = Users.findOne({_id: userData.id})
			
		.then(result => {

			result.enrollments.push({
				courseId : request.body.id
			})

			result.save()
			.then(saved => true)
			.catch(error => false)

		})
		.catch(error => false)

		// Push to course document
		let isCourseUpdated = Courses.findOne({_id: courseId})
		
		.then(result => {

			result.enrollees.push({userID: userData.id})
			result.save()
			.then(saved => true)
			.catch(error => false)

		})
		.catch(error => false)

		// If condition to check whether we have updated the users document and the course document

		if(isUserUpdated && isCourseUpdated){ 
			return response.send(true)
		}else{
			return response.send(false)
		}

	}

}

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	Users.findOne({_id: userData.id})
	.then(data => response.send(data))
}