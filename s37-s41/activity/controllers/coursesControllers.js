const Courses = require('../models/Courses.js')
const auth = require('../auth.js')

// This controller is for the Course Creation
module.exports.addCourse = (request, response) => {

	const courseData = auth.decode(request.headers.authorization)

	if (courseData.isAdmin) {
		// Creating an object using the Courses Model
		let newCourse = new Courses({

			// Supply all the required fields declared in the Courses() model
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			isActive: request.body.isActive,
			slots: request.body.slots

		})

		newCourse.save()
		.then(save => response.send(true))
		.catch(error => response.send(false))
	}else{
		return response.send(false)
	}
	
}

// In this controller, we are goin to retrieve all of the courses in our database
module.exports.getAllCourses = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		
		Courses.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false))
		
	}else{
		return response.send(false)
	}	

}

// Controller for retrieving all active courses
module.exports.getActiveCourses = (request, response) => {

	Courses.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(false))

}

// Controller for retrieving the informatino of a single document using the provided params
module.exports.getCourse = (request, response) => {

	const courseId = request.params.courseId
	Courses.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(false))

}

// Controller for updating the document of our course
module.exports.updateCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	const courseId = request.params.courseId

	// Edit the description, price and name
	let updatedCourse = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	}

	if(userData.isAdmin){

		Courses.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(true))
		.catch(error => response.send(false))

	}else{
		return response.send(false)
	}

}

// Controller for archiving courses
module.exports.archiveCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	const courseId = request.params.courseId

	let archivedCourse = {
		isActive: request.body.isActive
	}

	if(userData.isAdmin){
		Courses.findByIdAndUpdate(courseId, archivedCourse)
		.then(result => response.send(true))
		.catch(error => response.send(false))
	}else{
		return response.send(false)
	}

}

// Controller for getting inactive courses
module.exports.getInactiveCourses = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		Courses.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}else{
		response.send(false)
	}

}