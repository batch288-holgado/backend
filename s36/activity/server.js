const express = require('express');
const port = 4000;
const app = express();
const mongoose = require("mongoose");
const taskRoutes = require("./routers/taskRoutes.js")

// Set up MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch288holgado.3ynebd7.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true})

// Check whether we are connected with our db 
const db = mongoose.connection

		db.on("error", console.error.bind(console, "Error, can't connect to the db!"))

		db.once("open", () => console.log('We are now connected to the db!'))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/tasks", taskRoutes); 
// this route/middleware is responsible for the routes in the task; it connects the server.js to the taskRoutes

/*
	NOTE!! 
	
	"localhost:port/tasks" will be the base link for the server in postman

	anything from the routes must be added to tasks 

	e.g. to use the route route.get("/uses", taskControllers.getAllTasks) -> localhost/tasks/uses

*/

if (require.main==module){
	app.listen(port, () => console.log(`The server is running at port ${port}!`))
}

module.exports = app;