const mongoose = require('mongoose');
// recall that mongoose is a package that allows the creation of schemas and manipulation of our database

// Schema (mongoose.schema({}))
const taskSchema = new mongoose.Schema({ // capital S si Schema!!!
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// Model mongoose.model("collectionName", schemaName)
const Task = mongoose.model("Task", taskSchema)

//module.exports is a way for NodeJS to treat this value as a package that can be used by other files.
module.exports = Task;