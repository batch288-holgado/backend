const Task = require('../models/tasks.js') // this will direct and connect us to the exported file in the models folder

// Controllers
	// This controller will get/retrieve all the document from the tasks collection

module.exports.getAllTasks = (request, response) => {
	Task.find({}) // the empty ({}) indicates ALL the documents in the collection
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
}
// the module.exports was added so that the getAllTasks variable function can be exportable

// Create a controller that will add data in our database

module.exports.addTasks = (request, response) => {
	Task.findOne({"name" : request.body.name})
	.then(result => {
		if (result !== null){
			return response.send("Duplicate Task")
		}else{
			let newTask = new Task ({
				"name" : request.body.name
			})

		newTask.save()

		return response.send("New task created!");

		}
	}).catch(error => response.send(error))
}

// Creating a controller that will delete tasks using a parameter
module.exports.deleteTask = (request, response) => {
	console.log(request.params.id);
	// a controller with a "request.params.id" should be connected/exported to a router with a "/:id"; NOT: sa URL ilalagay ang ID if you will use request.params.id, hindi sa body mismo ng frontend

	// Mongoose has a findByIdAndRemove() method that will look for a document with the same ID in the URL and remove/delete the document from MongoDB

	let taskToBeDeleted = request.params.id;
	Task.findByIdAndRemove(taskToBeDeleted)
	.then(result => {
		return response.send(`The document that has the _id of ${taskToBeDeleted} has been deleted`)

	}).catch(error => response.send(error))
}

// Getting a specific task by ID
module.exports.getSpecificTask = (request, response) => {
	
	console.log(request.params.id)

	let specificTask = request.params.id;
	Task.findById(specificTask)
	.then(result => {
		return response.send(result)
	}).catch(error => (error))

}

// Updating Status
module.exports.updateTask = (request, response) => {

	console.log(request.params.id)

	let updatedTask = request.params.id;
	Task.findByIdAndUpdate(updatedTask, {status: "Completed"})
	.then(result => {
		return response.send(result)
	}).catch(error => (error))

}