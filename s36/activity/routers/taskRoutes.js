const express = require("express"); 
// express is required since this is what will allow us to use our CRUD operations (GET/POST/PUT/DELETE)

const taskControllers = require('../controllers/taskControllers.js')
// we need the controllers, so we set this variable and require the route to the exportable controllers (module.export.getAllTasks)

const router = express.Router()
// Contain all the endpoints of our application; we execute this code so that instead of typing app.get/app.post/app.put/app.delete, we write router.get/router.post/router.put/router.delete

// this is efficient since it connects it to the server.js

router.get("/", taskControllers.getAllTasks)
router.post("/addTask", taskControllers.addTasks)
// the 'taskControllers.controllerName' connects the router to the specific controller you want to connect it to

// Parameterizer
/*
	- We are going to create a route using a Delete method at the URL "/tasks/:id"

	- The colon here is an identifier that helps to create a dynamic route which allows us to supply information
*/
router.delete("/:id", taskControllers.deleteTask)
// this one will not return any response; it will stay on "sending request" since there is no response.send() here, however, since there is a console.log() in the controller connected, there will be a log in the terminal

router.get("/:id", taskControllers.getSpecificTask)
router.put("/:id/complete", taskControllers.updateTask)

module.exports = router;