// Use the "require" directive to load the express module/package

// It will allow us to access methods and functions that will help us easily create our application or server

// A 'module' is a software component or part of a program that contains one or more routines

const express = require('express')

// Create an application using express

	const app = express();
	// This creates an express application and stores this in a constant variable called app.

	const port = 4000;

// Middlewares
	// This is a software that provides common services and capabilities to applications outside of what is offered by the operating system

	app.use(express.json());
	// this allows our application to read json data

	app.use(express.urlencoded({extended: true}))
	
	// This will allow us to read data from forms since by default, information received from the URL can only be received as a string or an array

	// Applying the option of "extended : true" will allow us to receive information in other data types such as an object which will be used throughout the application


	// !!!!!!!!!! DO NOT FORGET THESE 2 MIDDLEWARES


// Running the Application
app.listen(port, () =>  console.log(`Server running at port ${port}`))
	// This tells the server to listen to the port; if the port is accessed, the server will run and a return message will be shown in the terminal confirming that the server is running

// [Section] Routes
	// Express has methods corresponding to each http method
	// this route expects to receive a GET request at the base URI "/"

	app.get("/", (request, response) => {

		// Once this route is accessed, it will send a string response containing "Hello World!"

		// compared to the previous session, response.end() uses the node.js modules method

		// express.js  modules isntead use the .send method to send a response back to the client; this is the equivaled of response.end() for node.js
		response.send('Hello World!');

	})

	// Adding another route; this route expects to receive a GET request at the URI "/hello"
	app.get("/hello", (request, response) => {
		response.send('Hellow from the /hello endpoint');
	})

	// This route expects to receive a POST request at the URI "/hello"
	app.post("/hello", (request, response) => {
		// request.body contains the contents/data of the request body from postman; all the properties defined in our POSTMAN request will be accessible here as properties with the same name
		console.log(request.body);
		response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`)
	})

	// This array will store user objects when the "/signup" route is accessed; this will serve as our mock database
	let users = [];

	app.post("/signup", (request, response) => {
		/*console.log(request.body);*/
 		
 		if(request.body.username !== "" && request.body.password !== ""){
 			console.log(`${request.body.username}, ${request.body.password}`)

 			users.push(request.body);

 			response.send(`User ${request.body.username} successfully registered!`)

 		}else{
 			response.send('Please input BOTH username and password.');
 		}	
	})

	// This route expects to receive a PUT request at the URI "change-password" which will update the password of a user that matches the informatino provided in the client/postman

	app.put("/change-password", (request, response) => {

		let message;

			for(let index = 0 ; index < users.length; index++){
					
				//If the provided username in the client/postman and the username of the current object in the loop is the same
				if(request.body.username == users[index].username){
							
					users[index].password = request.body.password

					message = `User ${request.body.username}'s password has been updated!`
						break;
					}else{
						message = 'User does not exist!'
					}

			}
			response.send(message);
		})

// Activity Section

app.get("/home", (request, response) => {
	response.send("Welcome to the home page")
})

app.get("/users", (request, response) => {
	response.send(users)
})

app.delete("/delete-user", (request, response) => {

	let message;

	for(let index = 0 ; index < users.length; index++){
				
		if(request.body.username == users[index].username){

		users.splice(index, 1)

		message = `User ${request.body.username} has been deleted`
		break;

		} else {
			message = 'User does not exist'
		}

	}

	if (message == undefined) {	
		message = 'No users found'
	} 

	response.send(message);
	console.log(message)

})

/*if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;*/