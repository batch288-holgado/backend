async function findName(db) {
	return await(

			db.users.find(
				{$or: [
					{firstName: {$regex: 's', $options: 'i'}}, 
					{lastName:{regex: 'd'}}
					]
				},
				{
					_id: 0,
					age: 0,
					email: 0,
					department: 0
				}
			)
		);

};

async function findDeptAge(db) {
	return await (

			db.users.find({
				$and:[
						{department: "HR"},
						{age: {$gte: 70}}
					]
			})
		);

};

async function findNameAge(db) {
	return await (

			db.users.find({
				$and: [
						{lastName: {$regex: 'e'}},
						{age: {$lte: 30}}
					]
			})
		);
};

try{
    module.exports = {
        findName,
        findDeptAge,
        findNameAge
    };
} catch(err){

};

