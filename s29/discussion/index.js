// Advanced Queries
	// Queries on embedded objects
		db.users.find({
			contact: {
				phone:"87654321",
				email: "stephenhawking@gmail.com"
			}
		}); // if you will find embedded objects, the object has to be EXACTLY the same

		db.users.find({
			contact: {
				email: "stephenhawking@gmail.com"
			}
		}); // this will not return anything, instead. use the dot notation instead for this to work

		db.users.find({
			"contact.email" : "stephenhawking@gmail.com"
		}); // this will give you what you're looking for 

	// Querying an array with an exact element
		db.users.find({
			courses: ["CSS", "Javascript", "Python"]
		}); // Input the field and the EXACT array

		db.users.find({courses: {
			$all: ["React"]
		}}) // use arrayName: {$all: ["elementName"]} in order to find all possible documents with an array having elementName
		/*
			db.users.find({arrayName: {
				$all: ["elementName"]
			}})
		*/

// Query Operators
// [Section] Comparison Query Operators
	// $gt or $gte
		/*
			- Allows us to find documents that have field number values that are greater than or equal to a specified value

			- Note that this operator will work only if the data type of the field is a NUMBER or an INTEGER

			Syntax:
				
				db.collectionName.find({field: {
					$gt: value
				}})

				db.collectionName.find({field: {
					$gte: value
				}})
		*/
		db.users.find({age: {$gt: 76}});
		db.users.find({age: {$gte: 76}});

	// $lt or $lte
		/*
			- Allows us to find documents that have field number values that are less than or equal to a specified value

			- Also works only if the data type of the field is a number or an integer

			Syntax:
				
				db.collectionName.find({field: {
					$lt: value
				}})

				db.collectionName.find({field: {
					$lte: value
				}})
		*/
		db.users.find({age: {$lt: 76}});
		db.users.find({age: {$lte: 76}});

	// $ne operator
		/*
			- "Not Equal" operator; allows us to find documents that have field number values not equal to a specified value

			Syntax:
				db.collectionName.find({field: {$ne : value}})
		*/
		db.users.find({age: {$ne:76}})

	// $in operator
		/*
			- Allows us to find documents with a specific match criteria of one field using different values

			Syntax: 
			db.users.find({
				field: 
					{
						$in: [value1, value2]
					}
			})
		*/
		db.users.find({lastName: {$in : ["Hawking", "Doe"]}});
		db.users.find({"contact.phone": {$in : ["87654321"]}})
		db.users.find({courses: {$in: ["React"]}});
 
// [Section] Logical Query Operators
	// $or operator
	  	/*
			- Allows us to find documents that match a single criteria from multiple provided search criteria

			- Multiple operators are also possible

			Syntax:
				db.collectionName.find({
					$or: [{fieldA: valueA}, {fieldB: valueB}]
				})
	  	*/
		db.users.find({$or: [{firstName: "Neil"}, {age: 25}]})
		db.users.find({ $or: [{firstName: "Neil"}, {age: {$gt:25}}]})
		db.users.find({
			$or:
			[
				{firstName: "Neil"},
				{age: {$gt:25}},
				{courses: {$in: ['CSS']}}
			]
		})

	// $and operator
		/*
			- Allos us to find documents matching ALL the multiple criteria in a single field

			Syntax:
			db.collectionName.find({
				$and:
				[
					{fieldA: valueA},
					{fieldB: valueB},
					...
				]
			})
		*/
		db.users.find({
			$and:
			[
				{age: {$ne:82}},
				{age: {$ne:76}}
			]
		});

// Mini-Activity
	// Query all the documents  from the users collection that follow the criteria:
	/*
		- Information of the documents older than 30, enrolled in CSS or HTML
	*/
db.users.find({
	$and:
	[
		{age: {$gt:30}},
		{courses: {$in: ["HTML", "CSS"]}}
	]
})

// [Section] Field Projection
	/*
		- Retrieving documents are common operations that we do and by defualt, mongoDB queries return teh whole document as a response
	*/

	// Inclusion
		/*
			- Allows us to include or add specific fields only when retrieving documents

			Syntax:
			db.collectionName.find(
				{criteria}, 
				{field : 1}
			)
		*/
		db.users.find(
			{firstName: "Jane"},
			{
				firstName: 1,
				lastName: 1,
				contact: 1,
				_id: 0
			}
		) // 1 means "on" and 0 means "off"

		db.users.find(
			{firstName: "Jane"},
			{
				firstName: 1,
				lastName: 1,
				"contact.phone": 1,
				_id: 0
			}
		)

	// Exclusion
		/*
			- Allows us to exclude/remove specific field only when retrieving documents

			Syntax:
			db.users.find(
				{criteria},
				{field: 0}
			)
		*/
		db.users.find(
			{firstName: "Jane"},
			{
				contact: 0,
				department: 0		
			}
		)

		db.users.find(
			{$or: 
				[{firstName: "Jane"}, {age:{$gt:30}}]
			},
			{
				"contact.email" : 0,
				department:0
			}
		)

	// Evaluation Query Operator
		// $regex operator
			/*
				- This allows us to find documents that match a specific string pattern using regular expressions

				Syntax:
					db.users.find({
						field: $regex: 'pattern', option: 'optionValue'
					})
			*/

			// Case Sensitive
			db.users.find({
				firstName: {$regex: 'N'}
			}); // This will return all documents with a 'N' in the first name

			// Case Insensitive
			db.users.find({
				firstName: {$regex: 'N', $options: 'i'}
			}) // $options: 'i' will make it case insenstive