// Array Methods
	// JavaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// [Section] Mutator Methods
	/*
		- Mutator methods are functions that "mutate" or change an array after they are created

		- These methods manipulate the original array performing various operations such as adding or removing element.
	*/

	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit']

	// .push() Method
		/*
			- Adds an element in the end of an array and returns the updated array's length
				
			Syntax: arrayName.push()
		*/

		console.log("Current Array: ");
		console.log(fruits);

		fruits.push("Mango");
			
		console.log(fruits.length);
		console.log("Mutated array from push method: ");
		console.log(fruits);

		// Pushing multiple element to an array

		fruits.push('Avocado', 'Guava')
		console.log('Mutated array after pushing multiple elements: ');	
		console.log(fruits);
		console.log(fruits.length);

	// .pop() Method
		/*
			- Removes the last element AND returns the removed element
			Syntax: arrayName.pop()
		*/

		console.log("Current Array: ");
		console.log(fruits);

		fruits.pop()
		console.log("Mutated Array from the pop method: ")
		console.log(fruits)

		let removedFruit = fruits.pop()
		console.log(removedFruit);

	// .unshift() Method
		/*
			- Adds one or more element at the beginning of an array
			Syntax: arrayName.unshift('elementA')
			if multiple = arrayName.unshift('elementA', 'elementB', ...)
		*/

		console.log("Current Array: ");
		console.log(fruits);

		fruits.unshift('Lime', 'Banana');
		console.log("Mutated array from unshift method: ");
		console.log(fruits);

		fruitsLength = fruits.unshift('Lime', 'Banana');
		console.log(fruitsLength)

	// shift() Method
		/*
			
			- Removes an element at the beginning of an array AND returns the removed element
			
			Syntax: arrayName.shift()

		*/

		console.log("Current Array");
		console.log(fruits);

		removedFruit = fruits.shift()
		console.log(removedFruit)

		fruits.shift()
		console.log("Mutated array from shift method: ")
		console.log(fruits)
		console.log(fruits.shift())

	// splice() Method
		/*
			- Simultaneously removes an element from a specified index and adds a new element

			Syntax: arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)

				- startingIndex: Target element to edit
				- deleteCount: How many elements from the target element to be deleted
				- elementsToBeAdded: Elements to be added

		*/

		console.log("Current Array: ");
		console.log(fruits);

		fruits.splice(1, 1, "eypol", "cheyri")
		console.log("Mutated array after the splice method: ")
		console.log(fruits)

		fruits.splice(fruits.length-1, 1, "cherry");
		console.log(fruits)

	// sort() Method
		/*
			- Rearranges the array elements in alphanumeric order (uppercase also before lowercase)
			Syntax: arrayName.sort()
		*/

		console.log("Current Array: ");
		console.log(fruits);

		fruits.sort();

		console.log("Mutated array from the sort method: ");
		console.log(fruits);

		// reverse() Method
			/*
				- Reverses the order of array elements
					Syntax: arrayName.reverse()
			*/

			console.log("Current Array: ");
			console.log(fruits);

			fruits.reverse();

			console.log("Mutated array from the reverse method: ");
			console.log(fruits);

// [Section] Non-Mutator Methods
	/*
		- Non-mutator methods are functions that do not modify or change an array after they are created

		- These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
	*/

	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

	// .indexOf()
		/*
			- Returns the index number of the first matching element found in an arary

			- If there is no match found, the result will be "-1" 

			- The search process will be done from the first element, proceeding to the last element

			Syntax:
				arrayName.indexOf(searchValue);
				arrayName.indexOf(searchValue, startingIndex);
		*/

		let firstIndex = countries.indexOf('PH')
		console.log(firstIndex)

		let invalidCountry = countries.indexOf('BR')
		console.log(invalidCountry)

		firstIndex = countries.indexOf('PH', 2);
		console.log(firstIndex) //the 'PH' means that you're looking for it, whereas the 2 means you start at index 2 ('CAN')

	// lastIndexOf()
		/*
			- Returns the index number of the last matching element found in an array

			- The search process will be done from the last element proceeding to the first element

			Syntax: arrayName.lastIndexOf(searchValue);
			arrayName.lastIndexOf(searchValue, startingIndex);

			NOTE that the index numbers remain (first element is still index = 0)
		*/

		let lastIndex = countries.lastIndexOf('PH')
		console.log(lastIndex)

		let invalidCount = countries.lastIndexOf('BR')
		console.log(invalidCountry)

		lastIndexOf = countries.lastIndexOf('PH', 4)
		console.log(lastIndexOf)

	/*
		- indexOf() - starting from the left -> right
		- lastIndexOf() - starting from right -> left
	*/

	// .slice()
		/*
			- Portions/Slices elements from an array AND returns a new array
			Syntax:
				arrayName.slice(startingIndex); -> start to end 
				arrayName.slice(startingIndex, endingIndex);
		*/

		// Slicing of elements from a specified index to the last element

		console.log(countries)

		 let slicedArrayA = countries.slice(2);
		 console.log("Result from slice method: ");
		 console.log(slicedArrayA);

		 // Slicing of elements from a specified index to another index:
		 let slicedArrayB = countries.slice(2, 4);
		 console.log("Result from the slice method: ");
		 console.log(slicedArrayB)

		 	/*
		 		- Elements to be sliced are the elements from the startingIndex until the element BEFORE the endingIndex 
		 		
		 		- .slice(2,4) is from index 2-3 only
		 		- .slice(3,6) is from index 3-5 only
		 	*/

		 	// Slicing off elements starting from the last element of an array
		 	let slicedArrayC = countries.slice(-3);
		 	console.log('Result from the slided method: ')
		 	console.log(slicedArrayC)

	// .toString()
		/*
			- Returns an array as a single string separated by commas
			Syntax: arrayName.toString();
		 */

		let stringArray = countries.toString();
		console.log(countries)
		console.log("Result from toString method: ")
		console.log(stringArray)
		console.log(typeof stringArray)

	// .concat()
		/*
			- Combines arrays to an array or elements and returns the combined result.

			Syntax: arrayA.concat(arrayB);, arrayB.concat(arrayA);
		*/

		let tasksArrayA = ["drink HTML", "eat JavaScript"];
		console.log("tasksArrayA: ")
		console.log(tasksArrayA)

		let tasksArrayB = ["inhale CSS", "breathe SASS"];
		console.log("tasksArrayB: ")
		console.log(tasksArrayB)

		let tasksArrayC = ["get Git", "be Node"];
		console.log("tasksArrayC: ")
		console.log(tasksArrayC)

		let tasks = tasksArrayA.concat(tasksArrayB)
		console.log("tasksArrayA.concat(tasksArrayB): ")
		console.log(tasks)

		let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
		console.log(combinedTasks)

		// concat multiple arrays into a single array
		let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC)
		console.log(allTasks)

	// .join()
		/*
			- Returns the array as a string, separated by a specified separator string

			Syntax: arrayName.join("separatorString")
		*/

		let users = ['John', 'Jane', 'Joe', 'Robert'];

		console.log(users.join());
		console.log(users.join(''));
		console.log(users.join(" - "));

// [Section] Iteration Methods
	/*
		- Loops designed to perform repetitive tasks
		- Iteration methods loops over all items in an array
	*/

	// .forEach()
		/*
			- Similar to a for() loop that iterates on each of the array element, except here, there is no longer need to set parameters.

			Syntax: arrayName.forEach(function(indivElement){statement})
		*/

		console.log(allTasks);

		allTasks.forEach(function (task) {
			console.log(task)
		})

		// each of the elements will be = task per loop; the last element will therefore be the last loop
		
		// filteredTasks variable will hold all the elements from the allTasks array that has > 10 characters
		let filteredTasks = []

		allTasks.forEach(function (task) {
			if(task.length > 10){
				filteredTasks.push(task)
			}
		})

		console.log(filteredTasks)
		
	// .map()
		// Almost the same as .forEach(), except this one returns a new array with different values depending on the results of the function's operation

		let numbers = [1, 2, 3, 4, 5];
		console.log(numbers)

		let numberMap = numbers.map(function(number){
			return number*number;
		})
		console.log(numberMap)
		// number became the individual elements inside the array

	// .every() 
		/*
			- This checks if all elements in an array meet the given condition
			- It will return "true" if ALL elements meet the condition, otherwise, it will return "false"
			Syntax: let/const resultArray = arrayName.every(function (indivElement){return expression/condition})
		*/

		numbers = [1, 2, 3, 4, 5];

		let allValid = numbers.every(function (number) {
			number < 3
		})

		console.log(allValid)

	// .some()
		/*
			- Checks if AT LEAST 1 element in the array meets the given condition

			Syntax: let/const resultArray = arrayName.some(function(indivElement){
				return expression/condition;
			})
		*/
		let someValid = numbers.some(function(number){
			return number < 3
		})
		console.log(someValid)

	// .filter()
		/*
			- Returns a new array that contains elements which meets the given condition

			- Returns an empty array if no elements meet the given condition

			Syntax: 
				let/const resultArray = arrayName.filter(function(indivElement){
					return expression/condition
				})
		*/

		numbers = [1, 2, 3, 4, 5];
		let filterValid = numbers.filter(function(number){
			return number % 2 === 0;
		})
		console.log(filterValid)

	// .includes()
		// Checks if the argument passed can be found in the array

		let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
		let productFound1 = products.includes('Mouse')
		console.log(productFound1)

	// .reduce()
		// Evaluates elements from left to right and returns/reduces the array into a single value

		let reduceArray = numbers.reduce(function(x, y){
			console.log('Accumulator ' + x)
			console.log('currentValue: ' + y)
			return x + y
		})
		//  x is the accumulator; y is the current value; basically this just creates a loop that performs the condition/expression in the function throughout all the elements

		console.log(reduceArray)

		/*
			[1,2,3,4,5] ->
			x=1, y=2
			accumulator: 1
			cuurentValue: 2 -> 2+1 -> accumulator: 3 +
			cuurentValue: 3 = 6...
		*/

		products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
		let reducedArrayString = products.reduce(function(x, y){
			return x + y
		})
		console.log(reducedArrayString)