// An array in programming is simply a list of data that shares the same data type

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// With an array, we can simply write the code above like this:

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']

// [Section] Arrays
	/*

		- Arrays are used to store multiple related values in a single variable.

		- They are declared using the square brackets([]), also known as "Array Literals"

		Syntax:
			let/const arrayName = [elementA, elementB, elementC...]

	*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Toshiba'];

// Possible use of an array, but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}] // this will not cause an error, but it is not the best way to practice

console.log(grades)
console.log(computerBrands)
console.log(mixedArr)

// Alternative way to write arrays:

let myTasks = [
		'drink HTML',
		'eat JavaScript',
		'inhale CSS',
		'bake SASS'
	];
console.log(myTasks)

// Creating an array with values from variable
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];

console.log(cities);

// [Section] Length Property
	// The .length property will give the total number of items in an array
	
	console.log(myTasks.length)
	console.log(cities.length)
	let blankArr = []
	console.log(blankArr)

	// The length property can also set the total number of elements in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length of an array

	myTasks.length -= 1
	// recall that x -= y -> x(newValue) = x(originalValue) - y, so in this case, what happens is myTasks.length(newValue) = myTasks.length(oldValue) - 1
	console.log(myTasks);
	console.log(myTasks.length);

	// To delete a specific item in an array, we can employ array methods (which will be shown/discussed in the next session) or an algorithm
	// Another example using decrementation:
	console.log(cities);
	cities.length--;
	console.log(cities);

	// We cannot do the same with strings; shortening the array by setting the property does not mean you can also shorten the number of characters using the same property
	let fullName = "Jamie Noble";
	console.log(fullName);
	fullName.length -= 1;
	console.log(fullName)

	// Since shortening an array is possible, lengthening it also is possible. This will forcibly add another item in the array which will be empty or undefined
	let theBeatles = ["John", "Ringo", "Paul", "George"];
	console.log(theBeatles);
	theBeatles.length += 1;
	console.log(theBeatles);

// [Section] Reading from Arrays
	/*

		- Accessing array elements is one of the more common tasks that we can do with an array
		- We can use this by using the array indexes
		-Each element is associated with its own index/number

		Syntax: arrayName[indexNumber]

	*/

	console.log(grades[0])
	console.log(computerBrands[2])

	let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
	console.log(lakersLegends);

	// 0: Kobe, 1: Shaq, 2: Lebron, 3: Magic, 4: Kareem

	let currentLaker = lakersLegends[2];
	console.log(currentLaker)

	// You can reassign array values using their respective index

	console.log("Array before reassignment: ");
	console.log(lakersLegends);

	lakersLegends[1] = "Pau Gasol";
	console.log("Array after reassignment: ")
	console.log(lakersLegends)

	// Accessing the last element of an array
		// Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one, allowin gus to get the last element
	
	let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
	console.log(bullsLegends[bullsLegends.length-1])

	// Adding elements into the array
	let newArr = []
	console.log(newArr)
	
	newArr[newArr.length] = "Cloud Strife";
	console.log(newArr)

	newArr[newArr.length] = "Tifa Lockhart";
	console.log(newArr)

	// Recall: The .length property is the number of ELEMENTS in the index, whereas the INDEX of the certain element is always -1 from its element number since index count starts at 0. The last index of the last element of the array is always .length-1. Therefore the arrayName[arrayName.length] will let us assign a NEW element, whereas arrayName[arrayName.length-1] will just be an reassignment

// [Section] Looping over an array
	// You can use a for loop to iterate over all items in an array

	// for loop
	for (index =  0; index < bullsLegends.length; index++){
		console.log(bullsLegends[index]);
	}
		// what happened here is that the loop stopped an bullsLegends[4] which was Kukoc, since bullsLegends.length = 5 and 4 < 5

	// Creating a loop that checks whether the elements inside the array is divisible by 5 or not

	let numArr = [5, 12, 30, 40, 46];
	let numDivBy5 = [];
	let numNotDivBy5 = [];

	for (index = 0; index < numArr.length; index++){
		if (numArr[index] % 5 === 0) {
			console.log(numArr[index] + " is divisible by 5")
			numDivBy5[numDivBy5.length] = numArr[index]
		}
		else {
			console.log(numArr[index] + " is not divisible by 5")
			numNotDivBy5[numNotDivBy5.length] = numArr[index]
		}
	}

	console.log(numDivBy5)
	console.log(numNotDivBy5)

// [Section] Multidimensional Arrays
	/*
		- These are useful for storing complex data structures
		- Though useful in a number of cases, these structures are not always recommended
	*/

	let chessboard = [
		['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
		]

console.log(chessboard)
console.log(chessboard[0][2]) // to access c1
console.log(chessboard[3][4]) // to access e4

// Two dimensional array
console.table(chessboard)

