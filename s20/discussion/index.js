// [Section] While Loop
	/*

	- A while loop takes an expression/condition. Expressions are any unit of code that can be evaluated asd true or false. If the condition evaluates to be a true statement, it will continute to be executed
	
	- A loop will iterate a certain number of times until an expression/condition is true

	- Iteration is the term given to repetition of statements

	Syntax:
		while(expression/condition){
			statement;
			increment/decrement;
		}

	*/

	let count = 5

	// While the value of count is !== 0, the statement inside will run/iterate
	while(count !== 0){
		
		// This prints our the current value of count
		console.log("While: " + count);

		// This decreases the value of count by 1 after every iteration to stop the loop when it reaches 0; forgetting this will make our applications run an infinite loop and eventually lead to a crash.
		count--;
	}

// [Section] Do While Loop

	/*
		- A do while loop works a lot like the while loop. But unlike while loops, do while loops guarantee that the code will be executed at least once.

		Syntax:
			do{
				statement;
				increment/decrement;
			} while (codition/expression)

	*/

	/*let number = Number(prompt("Give me a number"))
		// The Number() function works similarly to the "parseInt" function

	do {
		console.log("Do While: " + number);

		// Increment
		number++
	} while(number < 10)*/

// [Section] For Loop
	
	/*

		- A for loop is more flexible than while and do-while loops. It consists of 3 parts:

			1. The "initialization" value - tracks the progression of the loop
			2. The "expression/condition" that will be evaluated and will determine whether the loop will run one more time
			3. The "iteration" which will indicate how to advance the loop

			Syntax:
			for (initialization; expression/condition; iteration){
				statement;
			}

	*/

	for (let count = 0; count <= 20; count += 2){
		console.log("The current value of count is: " + count)
	}
	// replacing count++ to count + 2 will lead to an infinite loop since it will just lead to repeated 0+2. Count needs to be reassigned by count += 2 in order to make the iterations into intervals of 2

	let myString = "alex"
	// .length property
	// Characters in strings may be counted using the .length property
	console.log(myString.length)

	// Accessing elements of a string to the characters of the string.
	console.log(myString[0]); // This shows that index 0 in alex is letter "a"
	console.log(myString[1]);
	console.log(myString[2]);
	console.log(myString[3]);
	console.log(myString[5]); // Since the last index in our string is 3, this value will be undefined

	// Creating a loop that will print out the individual letters of the myString variable
	for (let index = 0; index < myString.length; index++){
		console.log(myString[index])
	}

	// Creating a string named "myName" with a value of Alex
	let myName = "Alex"
		/*
			Create a loop that will print out the letters of the name individualy and printout the number 3 instead if the letter is a vowel
		*/

	for(index = 0; index < myName.length; index++){
	
		// Basically, what we did here is that we used index to use as numbers
		if(myName[index].toLowerCase() === "a" || 
			myName[index].toLowerCase() === "e" || 
			myName[index].toLowerCase() === "i" || 
			myName[index].toLowerCase() === "o" || 
			myName[index].toLowerCase() === "u"){
			console.log(3)
		}
		else{
			console.log(myName[index]);
		}
	}

// [Section] Continue and Break Statements
	/*
	
		- The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

		- The "break" statement is used to terminate the current loop once a match has been found

		- In other words, using "continue" and "break" will both STOP/nullify/change the code it is applied to. The difference is that if you use "continue", the following codes will continue to run if applicable. If "break" is used, it stops immediately on the first encounter of the condition.

		- Create a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop

	*/

	for (let count = 0; count <= 20; count++){

		if(count>=10){
			console.log("The number is equal to 10, stopping the loop");
			break;
		}

		if(count % 2 === 0){
			console.log('The number is divisible by two, skipping ...')
			continue;
		}
		console.log(count)
	}
	
	let name = "alexandro"
		// Console the letters per line, and if the letter is "a", we are going to console "continue to the next iteration" then add continue statement.
		// If the current letter is equal to "d", we are going to STOP the loop

	for (let index = 0; index < name.length; index++){

		if (name[index] === "a"){
			console.log("Continue to the next iteration");
			continue;
		}

		else if (name[index] === "d"){
			break;
		}

		else{
			console.log(name[index]);
		}
	}