// Functions
	/*
		- Functions in JavaScript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
		
		- Functions are mostly created to create complicated tasks to run several lines of code insuccession.
	*/

// Function Declaration
	/*
		- Defines a function with the specified parameters
		- Syntax:
			function funcitonName(){
				codeBlock/statements;
			}
		- Function Keyword - Used to define a JavaScript function
		- functionName - the function name. Functions are name for use later in the code for the invocatin
		- Function Block ({}) - the statements which comprise the body of the function. This is where the code will be executed.
	*/

	// Example
		function printName(){
			console.log("My name is John!");
		}

// Function Invocation
	/*
		- The code block and statements inside a function are not immediately executed when the function is defined. 
		- The code block and the statements inside the functino are executed when the functino is invoked or called.
		- It is common to use the term "call a function" instead of "invoke a function"
	*/

	// Example
		printName();

// Function Declaration vs Function Expression
	// Function Declaration
		/*
			- A function can be created through functino declaration by using the functino keyword and adding the function name
			- Declared functions can be hoisted. As long as the function has been defined 
		*/

		declaredFunction();
		// Note: Hoisting is a JavaScript behavior for certain variables and functions to run or use before their declaration

		function declaredFunction(){
			console.log("Hello World from declaredFunction")
		};

	// Function Expression
		/*
			- A function can also be stored in a variable
			- A function expression is an anonymous function assigned to a variable function
			- Wala siyang declaredFunction
			- Basically, we put a "let/const functionName = function" instead
			- This cannot be hoisted
		*/

		let variableFunction = function(){
			console.log("Hello Again!");
		};

		variableFunction();

		// We can also create a function expression of a named function

		let funcExpression = function funcName(){
			console.log("Hello from the other side!")
		}

		funcExpression();

		// Declared and expressed functions can also be reassigned to new and anonymous functions

			// Example 1
			declaredFunction = function(){
				console.log("Updated declaredFunction!")
			};

			declaredFunction();

			// Example 2
			funcExpression = function(){
				console.log("Updated funcExpression!")
			};

			funcExpression();

			// However, we cannot re-assign a functino expression initialized with "const" (just like variables)

			const constantFunc = function(){
				console.log("Initialized with const!")
			};

			constantFunc();

			/*
				let constantFunc = function(){
					console.log("CANNOT BE REASSIGNED")
				}

				constantFunc();
			*/

// Function Scoping
	// Scoping is the accessibility of the function in our program

	/*
		JavaScript Variables have 3 types of scope:
		1. Global
		2. Local/Block Scope
		3. Function Scop
	*/

	{
		let localVar = "Armando Peres";
		console.log(localVar);
	}
	// console.log(localVar) -> error to kasi outside the code block eh local scope siya

	let globalVar = "Mr. Worldwide";
	{
		console.log(globalVar)
	}

	// Function Scope
		// JS has a function scope wherein each function creates anew scope
		// Variables defined inside a function are not accessible (visible) from outside the function

		function showNames(){
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionConst);
			console.log(functionLet);
		}

		/*console.log(functionConst);
		console.log(functionLet);
		these will result to error*/

		showNames();

	// Nested Functions
		// Functions inside a function

		function myNewFunction(){
			let name = "Jane";

			// Nested Function
			function nestedFunction(){
				console.log(name) // this will still work since global scoping na siya
			}
			nestedFunction(); // this will be an error if you invoke it outside the {}; the parent function must still be invoked in order for this to work
		}
		myNewFunction();

		let functionExpression = function(){
			function nestedFunction(){
				let greetings = "Hello Batch 288!";
				console.log(greetings);
			}
			nestedFunction();
		}
		functionExpression();

// Return Statement
	// The return statement allows us to output a value from a function to be passed to the line/block of code that invokes/calls the function

	function returnFullName(){
		let fullName = "Jeffrey Smith Bezos";
		let returnArray = [1, 2, 3, 4, 5]
		return(returnArray);
	}

	returnFullName();
	console.log(returnFullName())
	// in order for return function to show, the value inside console.log() must be the name of the function with the "()" inside

	let fullName = returnFullName()
	console.log(fullName)

// Function Naming Conventions
	// Function names should be definitive of the task it will perform. It usually containtsd a verb.

	function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"];
		return courses;
	}
	getCourses()
	console.log(getCourses())

	// Avoid generic names to avoid confusion within our code

	function get(){
		let name = "Jamie";
		return name;
	}
	get()
	console.log(get())

	// Avoid pointless and inappropriate function names

	function foo(){
		return 25%5;
	}

	// Name your functionsin lower case. Follow camelCame when naming variables and functions with multiple words

	function displayCarInfo(){
		console.log("Brand: Toyota")
		console.log("Type: Toyota")
	}
	displayCarInfo()