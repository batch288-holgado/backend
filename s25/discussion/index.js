// [Section] JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- Also used in other programming language, hence the name
	- Core JavaScript has a built-in JSON object that containes methods for parsing objects and converting strings into JavaScript objects
	- .json file, JSON file
	- JS Objects are NOT to be confused with JSON
	- JSON is used for serializing different data types into bytes
	- Serialization is the process of converting data into series of bytes for easier transmission/transfer of information/data
	- Byte: Unit of data with 8 binary digits (1,0) that is used to represent a character (letters, numbers, typographin symbols).

	Syntax: {
		"propertyA" : "valueA"
		"propertyB" : "valueB"
	}
*/

// [Section] JSON Arrays
/*
	"cities" : [
		{"city" : "Quezon City", "province" : "Metro Manila", "country" : "Philippines"}
	]
*/

// [Section] JSON Methods
/*
	- The JSON object contains methods for parsing and converting data into stringified JSON.
	- Converting Data into Stringified JSON
		- Stringified JSON is a JavaScript object converted into a string to be used in other functions of a JavaScript application
		- They are commonly used in HTTP request wehre informatino is requried to be sent and received in a stringified version
		- Requests are an important type of programming where an application communicates with a backend application to perofrom different tasks such as getting/creating data in a database.
*/
	
	let batchesArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'}];
	console.log(batchesArr);
	console.log(typeof batchesArr)

	// The stringify method is used to convert JavaScript Objects into a string
	console.log('Result from stringify method:');

	// To convert objects into strings, use JSON.stringify(variableName);
	console.log(JSON.stringify(batchesArr));
	console.log(typeof JSON.stringify(batchesArr))

	// Using stringify method with variables
	/*
		Syntax: 
		JSON.stringify({
			propertyA : variableA
			propertyB : variable B
		})
	*/

	// User Details
	/*let firstName = prompt('What is your first name?')
	let lastName = prompt('What is your last name?')
	let age = prompt('What is your age?')
	let address = {
		city: prompt('Which city do you live in?'),
		country: prompt('Which country does your city address belong to?')
	};

	let otherData = JSON.stringify({
		firstName: firstName,
		lastName: lastName,
		age: age,
		address: address
	});
	console.log(otherData)*/

	// Converting stringified objects into JavaScript Objects
		// Objects are common data types used in applications because of the complex data structures that can be created out of them; to convert strings into JS arrays/objects, use JSON.parse(variableName)

		let batchesJSON = '[{"batchName" : "Batch X"}, {"batchName" : "Batch Y"}]'
		console.log(batchesJSON)
		console.log(JSON.parse(batchesJSON))

		let exampleObject = '{"name" : "Chris", "age" : 16, "isTeenager" : false}'
		console.log(exampleObject);
		console.log(JSON.parse(exampleObject));
		let exampleParse = JSON.parse(exampleObject);

		console.log(exampleParse.name);
		console.log(typeof exampleParse.name);

		console.log(exampleParse.age);
		console.log(typeof exampleParse.age);
		
		console.log(exampleParse.isTeenager);
		console.log(typeof exampleParse.isTeenager);

		// NOTE: USE '' for parse, since the "" will be used to declare the properties/string values inside, but if entering, use `` instead
		let users = `[
			{
				"id" : 1, 
				"name" : "Leanne Graham",
				"username" : "Sincere@april.biz",
				"address" : {
					"street" : "Kulas Light",
					"suite" : "Apt. 556",
					"city": "Gwenborough",
					"zipcode": 929983874,
					"geo" : {
						"lat" : -37.3159,
						"lng": 81.1496
					}
				},
				"phone": "1-770-736-8031 x56442",
				"website": "hildegard.org",
				"company": {
				  "name": "Romaguera-Crona",
				  "catchPhrase": "Multi-layered client-server neural-net",
				  "bs": "harness real-time e-markets"
				}
			},
			{
				"id" : 2,
				"name": "Ervin Howell",
				"username": "Antonette",
				"email": "Shanna@melissa.tv",
				"address" : {
					"street" : "Victor Plains",
					"suite": "Suite 879",
					"city": "Wisokyburgh",
					"zipcode": 905667771,
					"geo": {
					  "lat": -43.9509,
					  "lng": -34.4618
					}
				},
				"phone": "010-692-6593 x09125",
				"website": "anastasia.net",
				"company": {
				  "name": "Deckow-Crist",
				  "catchPhrase": "Proactive didactic contingency",
				  "bs" : "synergize scalable supply-chains"
				}
			},
			{
				"id" : 3,
				"name" : "Clementine Bauch",
				"username" : "Samantha",
				"email" : "Nathan@yesenia.net",
				"address" : {
					"street" : "Douglas Extension",
					"suite" : "Suite 847",
					"city" : "McKenziehaven",
					"zipcode" : 595904157,
					"geo" : {
						"lat": -68.6102,
          	"lng": -47.0653
					}
				},
				"phone": "1-463-123-4447",
      	"website": "ramiro.info",
      	"company": {
      		"name": "Romaguera-Jacobson",
      		"catchPhrase": "Face to face bifurcated interface",
      		"bs": "e-enable strategic applications"
      	}
			},
			{
				"id": 4,
				"name": "Patricia Lebsack",
				"username": "Karianne",
				"email": "Julianne.OConner@kory.org",
				"address" : {
					"street" : "Hoeger Mall",
					"suite" : "Apt. 692",
					"city" : "South Elvis",
					"zipcode" : 539194257,
					"geo" : {
          "lat": 29.4572,
          "lng": -164.2990
        	}
				},
				"phone": "493-170-9623 x156",
				"website": "kale.biz",
				"company": {
				  "name": "Robel-Corkery",
				  "catchPhrase": "Multi-tiered zero tolerance productivity",
				  "bs": "transition cutting-edge web services"
				}
			},
			{
				"id": 5,
				"name": "Chelsey Dietrich",
				"username": "Kamren",
				"email": "Lucio_Hettinger@annie.ca",
				"address": {
				  "street": "Skiles Walks",
				  "suite": "Suite 351",
				  "city": "Roscoeview",
				  "zipcode": 33263,
				  "geo": {
				    "lat": -31.8129,
				    "lng": 62.5342
				  }
				},
				"phone": "(254)954-1289",
				"website": "demarco.info",
				"company": {
				  "name": "Keebler LLC",
				  "catchPhrase": "User-centric fault-tolerant solution",
				  "bs": "revolutionize end-to-end systems"
				}
			},
			{
				"id": 6,
				"name": "Mrs. Dennis Schulist",
				"username": "Leopoldo_Corkery",
				"email": "Karley_Dach@jasper.info",
				"address": {
					"street": "Norberto Crossing",
					"suite": "Apt. 950",
					"city": "South Christy",
					"zipcode": 235051337,
					"geo": {
					  "lat": -71.4197,
					  "lng": 71.7478
					}
				},
				"phone": "1-477-935-8478 x6430",
				"website": "ola.org",
				"company": {
				  "name": "Considine-Lockman",
				  "catchPhrase": "Synchronised bottom-line interface",
				  "bs": "e-enable innovative applications"
				}
			},
			{
				"id": 7,
				"name": "Kurtis Weissnat",
				"username": "Elwyn.Skiles",
				"email": "Telly.Hoeger@billy.biz",
				"address": {
				  "street": "Rex Trail",
				  "suite": "Suite 280",
				  "city": "Howemouth",
				  "zipcode": 588041099,
				  "geo": {
				    "lat": 24.8918,
				    "lng": 21.8984
				  }     
				},
				"phone": "210.067.6132",
				"website": "elvis.io",
				"company": {
				  "name": "Johns Group",
				  "catchPhrase": "Configurable multimedia task-force",
				  "bs": "generate enterprise e-tailers"
				}
			},
			{
				"id": 8,
				"name": "Nicholas Runolfsdottir V",
				"username": "Maxime_Nienow",
				"email": "Sherwood@rosamond.me",
				"address": {
				  "street": "Ellsworth Summit",
				  "suite": "Suite 729",
				  "city": "Aliyaview",
				  "zipcode": 45169,
				  "geo": {
				    "lat": -14.3990,
				    "lng": -120.7677
				  }
				},
				"phone": "586.493.6943 x140",
				"website": "jacynthe.com",
				"company": {
				  "name": "Abernathy Group",
				  "catchPhrase": "Implemented secondary concept",
				  "bs": "e-enable extensible e-tailers"
				}
			},
			{
				"id": 9,
				"name": "Glenna Reichert",
				"username": "Delphine",
				"email": "Chaim_McDermott@dana.io",
				"address": {
				  "street": "Dayna Park",
				  "suite": "Suite 449",
				  "city": "Bartholomebury",
				  "zipcode": 764953109,
				  "geo": {
				    "lat": 24.6463,
				    "lng": -168.8889
				  }
				},
				"phone": "(775)976-6794 x41206",
				"website": "conrad.com",
				"company": {
				  "name": "Yost and Sons",
				  "catchPhrase": "Switchable contextually-based project",
				  "bs": "aggregate real-time technologies"
				}
			},
			{
				"id": 10,
				"name": "Clementina DuBuque",
				"username": "Moriah.Stanton",
				"email": "Rey.Padberg@karina.biz",
				"address": {
				  "street" : "Kattie Turnpike",
				  "suite": "Suite 198",
				  "city": "Lebsackbury",
				  "zipcode": 314282261,
				  "geo": {
				    "lat": -38.2386,
				    "lng": 57.2232
				  }
				},
				"phone": "024-648-3804",
				"website": "ambrose.net",
				"company": {
				  "name": "Hoeger LLC",
				  "catchPhrase": "Centralized empowering task-force",
				  "bs": "target end-to-end models"
				}
			}]`

    console.log(JSON.parse(users))