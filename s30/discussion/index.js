// [Section] MongoDB Aggregation
	/*
		- Generated manipulated data and perform operations to create filtered results that help analyze date

		- Compred to doing the CRUD operations with our data from previous sessions, aggregation gives us access to maniuplate, filter and compute for results, providing us with information to make necessary development decisions without having to create a frontend application
	*/
	
// Using aggregate method (note: operators have to be in objects)
	
	// [Sub-Section] $match 
		// this $match property is like a filter in such a way that only the documents that satisfy the $match criteria are returned
			db.fruits.aggregate([
				{$match: {onSale: true}}
			])
		
			/*
				Syntax: {$match: {fieldName: value}}
			*/

	// [Sub-Section] $group
		// The $group operator groups the documents in terms of the property declared in the _id property

			db.fruits.aggregate([
				{$group: {_id: "$supplier_id"}}
			])
				// this means that the $supplier_id values will be where the succeeding operators will work on

			// $sum operator
			db.fruits.aggregate([
				{$group: {
					_id: "$supplier_id", 
					total: {$sum: "$stock"}
					}
				}
			])

			/*
				Syntax: 
					{$group: {
						_id: "$nameOfField",
						userDefinedProperty: {$operator: "$valueForOperation"}
						}
					}
			*/
				// basically, what happens here is that the documents with the same _id are grouped together and the sums of their respective $stock values are shown in the total

				// you may add extra operators to add extra information. in this case, we created a property named "total" and we created an object collecting the $sum of the $stock of the documents with the same $supplier_id

				// note that the "total" above is user defined; it can be anything, what's important is the operation inside

		// if both $match and $group are put together, the documents that satisfy $match will only be the documents to be affected by $group

			db.fruits.aggregate([
				{$match: {onSale:true}},
				{$group: 
					{_id: "$supplier_id"}
				}
			])

			db.fruits.aggregate([
				{$match: {onSale:true}},
				{$group: {
					_id: "$supplier_id", 
					total: {$sum: "$stock"}
					}
				}
			])

		// $max operator
			db.fruits.aggregate([
				{$match: {onSale:true}},
				{$group: 
					{
						_id: "$supplier_id", 
						sum: {$sum: "$stock"},
						max: {$max: "$stock"}
					}
				}
			]) // $max operator takes the max (highest) value associated with the stock property

	// [Sub-Section] $project
		/*
			- The $project operator can be used when aggregating data to exclude the returned result
		*/

		db.fruits.aggregate([
			{$match: {onSale:true}},
			{$group: 
				{
					_id: "$supplier_id", 
					sum: {$sum: "$stock"},
					max: {$max: "$stock"}
				}
			},
			{$project: {_id:0}}
		]) // basically, tinatanggal lang si _id

	// [Sub-Section] $sort
		/*
			- The $sort operator can be used to change the order of the aggregated results.

			- Providing a value of -1 will sort the aggregated results in a reversed order

			Syntax:
				{$sort: {field: 1/-1}} -> this means that the value of the field will be arranged in ASCENDING order (if field: 1) or in DESCENDING order (if field: -1)
		*/
		db.fruits.aggregate([
			{$match: {onSale:true}},
			{$group:
				{
					_id:"$supplier_id",
					total: {$sum: "$stock"}
				}
			},
			{$sort: {total:1}}
		])

	// [Sub-Section] $unwind
		/*
			- This operator creates new documents, giving each new document with one value from the array associated with $unwind.

			- In other words, the $unwind operator deconstructs an array field from a collection/field with an array value, and then assigns those values to new documents for each element

			Syntax: {$unwind: "$fieldName"}
		*/
		db.fruits.aggregate([
			{$unwind: "$origin"}
		])

		// Grouping Unwinded Documents
			db.fruits.aggregate([
				{$unwind: "$origin"},
				{$group:
					{
						_id:"$origin",
						kinds: {$sum:1} // {$sum:1} operator counts how many fields there are per group
					}
				},
				{$sort: {kinds:1}} // pwede rin {$sort: {_id:1}} if you want to arrange the countries alphabetically
			])

// Documentation: https://www.mongodb.com/docs/manual/reference/operator/aggregation/