async function fruitsOnSale(db) {
	return await(

			db.fruits.aggregate([
				{$match: {onSale:true}},
				{$count: "stock"}
			])

		);
};

async function fruitsInStock(db) {
	return await(

			db.fruits.aggregate([
				{$match: {stock:{$gt:20}}},
				{$count: "stock"}
			])
		);
};

async function fruitsAvePrice(db) {
	return await(

			db.fruits.aggregate([
				{$match: {onSale:true}},
				{$group:
					{
						_id: "$supplier_id",
						average: {$avg: "$price"}
					}
				}
			])
		);
};

async function fruitsHighPrice(db) {
	return await(

			db.fruits.aggregate([
				{$group:
					{
						_id: "$supplier_id",
						max: {$max: "$price"}
					}
				}
			])
		);
};

async function fruitsLowPrice(db) {
	return await(

			db.fruits.aggregate([
				{$group:
					{
						_id: "$supplier_id",
						min: {$min: "$price"}
					}
				}
			])
		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};
