function printInput(){
	let nickname = "Eliot";
	console.log("Hi, " + nickname)
}
printInput()

// In other cases, functions can also process data directly passed into them instead of relying only on Global Variables

// Consider this function:

function printName(name){
	console.log("My name is " + name);
};
printName("Eliot");
printName("Aurelius");
printName("Nathan");
// In other words, using these parameters will allow you to use the same function again and again  with different values.

// Variables can also pass as arguements
let sampleVariable = "Curry";
printName(sampleVariable);

let sampleArray = ["Messi", "Xavi", "Iniesta"]
printName(sampleArray)

// One example of using the Parameter and Argument
	// functionName(number);
	function checkDivisibilityBy8(num){
		let remainder = num % 8
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);

		let isDivisibleby8 = remainder === 0;

		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleby8);
	}
	checkDivisibilityBy8(64);
	checkDivisibilityBy8(50);
	checkDivisibilityBy8(7);

// Functions as Arguments
	// Function Parameters can also accept other functions as arguments

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed!");
}

function invokeFunction(func){
	func();
}

invokeFunction(argumentFunction)

// Function as Argument with "return" keyword
function returnFunction(){
	let name = "Chris";
	return name;
}

function invokedFunction(func){
	console.log(func());
}

invokedFunction(returnFunction)
	// returnFunction was not yet invoked, so the func() inside the console.log was its invocation na since returnFunction = func when it was invoked in invokedFunction(returnFunction)

// Using Multiple Parameters
	// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}
createFullName('Aurelius', 'Castillo', 'Holgado')

// In JavaScript, providing more/fewer arguments than the expected will not return an error
	
	// Providing fewer arguments than the expected parameters will automatically assign an undefined value to the parameter

	createFullName('Eliot', 'Holgado')

	// Proving more arguments than the expected parameters will not assign the argument anywhere
	createFullName('Aurelius', 'Castillo', 'Holgado', 'Eliot')

	// In other programming languages, this will return an error stating that "the number of arguments do not match the number of parameters".

	// Using variables as multiple arguments
	let firstName = 'John';
	let middleName = 'Doe';
	let lastName = 'Smith';

	createFullName(firstName, middleName, lastName);

// Alert() and Prompt()
	// alert() allows us to show a small window at the top of our browser page to show informtaion to our users; as opposed to console.log which only shows the message on the console.

	// alert("Hello World!"); -> this will run immediately when the page loads
	// Syntax: alert("messageInString")

	function showSampleAlert() {
		alert("Hello, user!");
	};

	// showSampleAlert()

	console.log("I will only log in the console when the alert is dismissed.");

	// Using prompt()
		// prompt() allows us to show a small window at the top of the browser to gather user input
		// Values gathered from prompts are always returned as string data types
		// Syntax: prompt("dialogInString")
		
		// let samplePrompt = prompt("Enter your name.");
		// console.log("Hello, " + samplePrompt);
		// console.log(typeof samplePrompt);

	/*	let age = prompt("Enter your age.");
		console.log(age);
		console.log(typeof age);*/

		// This will either return an empty string - when there is no input, or null - if the user cancels the prompt
		// let sampleNullPrompt = prompt("Don't enter anything.");
		// console.log(sampleNullPrompt);

		function printWelcomeMessage(){
			let firstName = prompt("Enter your first name.");
			let lastName = prompt("Enter your last name.");
		}
		
		printWelcomeMessage()
		
		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");

		