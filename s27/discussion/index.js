index.js 

/*
Users:
	ObjectID
	First Name (string)
	Last Name (string)
	Email (string)
	Password (string)
	Phone Number (string)
	Is Admin (Boolean) - if the value of this property is true, then the user is an admin, otherwise, regular customer
	Enrollments (Array) - this will be an array that contains the enrollments of the user

Courses:
	Name (string)
	Price (string)
	Description (string)
	Slots (string)
	isActive (Boolean) - if true, it means that the course is available, otherwise, not
	Enrollees (Array) - that contains the list of students/users that enroll the course

Transaction:
	Transaction ObjectID
	Course ObjectID
	User ObjectID
*/