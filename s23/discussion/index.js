// [Section] Objects
	/*
		- An object is a data type that is used to represent real world objects.

		- It is a collection of related data and/or functionalities
		- Structure of an object: Key = Property
	*/
	
// Creating using the object initialization / object literal notations
	/*
		Syntax: 
		let objectName = {
			keyA: valueA,
			keyB: valueB,
			...
		}
	*/
	
let cellphone = {
	// key-value pairs
	name: 'Nokia 3210',
	manufactureDate: 1999
}

console.log('Result from creating objects using initializers/literal notation')

let exampleArray = [1,2,3,4,5];
console.log(exampleArray) 
console.log(typeof exampleArray) //this will console.log an object, but if you click the dropdown button in the array, it will show there an object

console.log(cellphone)
console.log(typeof cellphone)
console.log(cellphone.length) // this will console.log an 'undefined' -> this is how you differentiate an object from an array

// Creating objects using a constructor function
	/*
		- Creates a reusable function to create several objects that have the same data structure.
		
		- This is useful for creating multiple instance/copies of an object; an instance is a concrete occurance which emphasizes on the distinct/unique identity

		Syntax:
		function objectName (valueA, valueB) {
			this.keyA = valueA;
			this.keyB = valueB;
		}
	*/
	
	// naming convention is always Captial on the first letter
	
	function Laptop (name, manufactureDate) {

		// "this" keyword allows us to assign a new object's properties by associating them with values received from a constructor's function parameters.

		this.name = name;
		this.manufactureDate = manufactureDate;
	}


	// Unique instance of the laptop Object
		/*
			- The "new" operator creates an instance of an object
			- Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
		*/

	let laptop = new Laptop('Lenovo', 2008);
	console.log('Result from creating objects using object constructors: ');
	console.log(laptop);

	let myLaptop = new Laptop ('Macbook Air', 2020);
	console.log('Result from creating objects using object constructors: ');
	console.log(myLaptop);

	/*
		- The example below invokes/calls the laptop function instead of creating a new object instance
		- Returns "undefined" without the "new" operator because the "Laptop" function does not have a return statement
	*/
	
	let oldLaptop = Laptop('Portal R2E CCMC', 1980);
	console.log('Result from creating objects using object constructors: ');
	console.log(oldLaptop);

	// to use in devtools, input: new Laptop ("", __)

// Creating Empty Objects
	let computer = {}
	let myComputer = new Object();


// [Section] Accessing Object Properties
	// Using the dot/.propertyName notation
	// Syntax: objectInstanceName.propertyName -> value of propertyName
	console.log("Result from dot notation: " + myLaptop.name);

	// Using square bracket notation (same lang with dot notation)
	// Syntax: objectInstanceName['propertyName'] -> value of propertyName
	console.log("Result from square bracket notation: " + myLaptop['name'])

		// myLaptop.name or myLaptop['name'] will be the value of the name property of myLaptop instance (Macbook Air); therefore, logging myLaptop.manufactureDate will log the value of the manufactureDate property of the myLaptop instance (2020)

// Accessing Array Objects
	/*
		- Accessing array elements can also be done using square brackets
		- Accessing object properties using the square bracket notation and array indexes can cause confusion
		- By using the dot notation, this easily helps us differentiate accessing elemetns from arrays and properties from objects
		- Object properties have names that make it easier to associate pieces of information
	*/

	let array = [laptop, myLaptop]
	console.log(array[0].name)
	console.log(array[0]['name'])

// [Section] Initializing / Adding / Deleting / Reassigning Object Properties
	/*
		- Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared

		- This is useful for times when an object's properties are undetermined at the time of creating them
	*/

	let car = {};
	// Initializing/Adding object properties using dot notation
	car.name = 'Honda Civic';
	console.log('Result from adding properties using dot notation');
	console.log(car)

	// Initializing/Adding object properties using square bracket notation
		/*
			- Using the square bracket notation to access object properties will allow access to spaces when assigning property names which make sit easier to read
		*/

	car["Manufacture Date"] = 2019
	
	console.log(car['Manufacture Date']);
	//logs 2019 as it should

	console.log(car['manufacture date']); 
	// logs undefined since it is case sensitive

	console.log(car.manufactureDate);
	// logs undefined since there is no property named manufactureDate

	console.log('Result from adding properties using square bracket notation: ');

	console.log(car);
	// logs all properties given to car, including the name given above, with the Manufacture Date

	// Deleting Object Properties
	delete car['Manufacture Date'] // can also be car.name
	console.log("Result from deleting properties: ")
	console.log(car)

	// Reassigning Object Properties
	car.name = 'Dodge Charget R/T'
	console.log('Result from deleting properties');
	console.log(car)

	car['name'] = 'Ford Territory'
	console.log(car)

// [Section] Object Methods
	/*
		- A method is a function which is a property of an object
		- They are also functions and one of the key differences they have is that methods are functions related to a specific object
		- Methods are useful for creating object specific functions which are sued to perform tasks on them
		- Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
	*/

let person = {
	name: 'John',
	talk: function () {
		console.log('Hello, my name is ' + this.name)
	}
}

console.log(person)
console.log('Result from object methods: ')
person.talk(); // input person.talk() sa dev tools kapag return instead of console.log

// Adding methods to objects
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward');
}
person.walk();

// Methods are useful for creating reusable functions that perform tasks related to objects

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		state: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@mail.xyz'],
	introduce: function(){
		console.log('Hello, my name is ' + this.firstName + this.lastName)
	}
}
friend.introduce()

// [Section] Real World Application of Objects
/*
	Scenarios:
		1. We would like to create a game that would have several pokemon interact with each other
		2. Every pokemon would have the same set of stats, properties and functions
*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled target pokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth")
	},
	faint: function(){
		console.log("Pokemon fainted")
	}
}

console.log(myPokemon);

// Using object literals to create multiple kinds of pokemon would be time consuming

// Creating an object constructor will help in this process

function Pokemon (name, level){

	// Properties
	this.name = name;
	this.level  = level;
	this.health = 2*level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name)
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	}

	this.faint = function(){
		console.log(this.name + ' fainted')
	}
}

// Create new instances of "Pokemon" object, each with their unique properties

let pikachu = new Pokemon ("Pikachu", 16)
let rattata = new Pokemon ("Rattata", 8)

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the 2 objects
pikachu.tackle(rattata)
