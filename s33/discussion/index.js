// [Section] JavaScript Synchronous vs Asynchronous
	// By default, JavaScript is synchronous, meaning only one statement is executed at a time

	// This can be proven when a statement has an error, JS will not proceed with the next statement if there is an error
		/*console.log("Hello World!");
		conole.log("Hello after the world");
		console.log("Hello");*/

	// When certain statements take a lot of time to process, this slows down our code
	
	/*
		for (let index = 0; index <= 10000000; index++){
			console.log(index);
		}

		console.log("Hello Again!")
	*/

	// Asynchronous means that we can proceed to execute other statements while consuming code is running in the background

// [Section] Getting All Posts
	// The Fetch API allows you to asynchronously request for a resource data
	// This means that the fetch method to be used here will run asychronously
	// A 'promise' is an object that represents the eventual completion (or failure) of an asychronous function and its resulting value
	
	/*
		Syntax: 
			fetch('URL');
			.then((response => response));
	*/
	
	// Retrieve all posts 
		fetch('https://jsonplaceholder.typicode.com/posts')
			// Using the fetch('url') method retrieves all the data responses from the database, but since it will take time, it will return a "promise" instead

		// .then(response => console.log(response))
			// The .then(parameter => function) will then catch the 'promise' from fetch; the .then processes the promise and returns a JSON object
			// Recall: if you use an arrow function on a 1 liner, there is already an implicit return (no need to type a return)

		/*.then(response => {
			return response.json()
		})*/
			// Use the "json" method from the response object to convert the data retrieved into JSON format to be used in our application. NOTE THAT THERE ARE NOT ;s SINCE FETCH AND .THEN SHOULD ALWAYS BE TOGETHER
			// response.json is returned since it is in another link
			// 'response' here is developer defined; it can be any other name

		// .then(response => console.log(response)) -> this one returns only a description of the data to be retrieved; parang preview lang

		.then(response => response.json())
			// this stores the data retrieved into the variable "resposne" and response.json() converts the data retrieved into a json object; this will however, still return a 'promise'

	
		.then(json => console.log(json))
			// json here is also user defined -> this .then() processes the response.json() 

		// these parameters are all function-scoped, meaning that you can use the same names again in another fetch('url') method

	// Using the "async" and "await" keywords is another approach that can be used to achieve an asynchronouse code

		/*
			This one returns a promisel; putting "await" before the fetch will process the promise

			async function fetchData(){
				let result = fetch('https://jsonplaceholder.typicode.com/posts');
				console.log(result)
			}; 
			fetchData();
		*/

		async function fetchData(){
			let result = await fetch('https://jsonplaceholder.typicode.com/posts');

			console.log(result);

			let json = await result.json();

			console.log(json);
		}; 
		fetchData();

// [Section] Getting Specific Posts
	// Retrieves a specific post following the rest API(/post/id)

	fetch ('https://jsonplaceholder.typicode.com/posts/1')

	.then(response => response.json()) 
	// if you console.log(response) here instead of response.json(), it will just show an overview/preview/description of the data you want to retrieve here, so another .then must be used to show all the details

	.then(result => console.log(result))

// [Section] Creating Post
	/*
		Syntax:
			fetch('url', {options}) -> 'options' is an object that contains the method, heads and body of the request; by default, if there is no 'options', the method is automatically a GET method
			
			.then(response => {})
			
			.then(response => {})
	*/
	fetch('https://jsonplaceholder.typicode.com/posts', {
		
		// this sets the method of the request object to POST the following API
		method: 'POST',
		
		// this sets the header data of the request object to be sent to the backend; 'application/json' specifies that the content will be in JSON structure
		headers: {
			'Content-type':'application/json'
		},

		// this sets the content/body data of the request object to be sent to the backend
		body: JSON.stringify({
			title: 'New post',
			body: 'Hello world',
			userID: 1
		}) // JSON.stringify() method is used to convert this into a JSON object instead of a normal object, since JSON ang nasa backend
	})

	.then(response => response.json())
	.then(json => console.log(json))

// [Section] Updating a Post -> note that for here, the URL must be specific and directed already to the specific document to be updated

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		headers: {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
			id: 1,
			title: 'Updated Post',
			body: 'Hello again!',
			userID: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))

	// Patch
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
			title: 'Updated Post',
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))

	// PATCH vs PUT
		/*
			- PUT method updates ALL elements inside; it is a method of modifying a resource where the client sends data and updates the ENTIRE object/document

			- PATCH method updates only what is declared; it applies only a PARTIAL UPDATE to the object or document
		*/

// [Section] Deleting a Post
	fetch('https://jsonplaceholder.typicode.com/posts/1', {method: 'DELETE'})
	.then(response => response.json())
	.then(json => console.log(json))