// 1. What directive is used by Node.js in loading the modules it needs?

	// Answer: "require"

// 2. What Node.js module contains a method for server creation?

	// Answer: let http = require('http')

// 3. What is the method of the http object responsible for creating a server using Node.js?

	// Answer: http.createServer(function(request, response){}).listen(xxxx) or let server = http.createServer((request, response) => {})

// 4. What method of the response object allows us to set status codes and content types?
	
	// Answer: response.writeHead()

// 5. Where will console.log() output its contents when run in Node.js?

	// Answer: gitBash

// 6. What property of the request object contains the address's endpoint?

	// Answer: response.end()

const http = require ('http')
const port = 3000
const server = http.createServer((request, response) => {

	if (request.url == '/login') {

		response.writeHead(200, {"Content-type" : "text/plain"})
		response.end('Welcome to login page.')

	} else {

		response.writeHead(404, {"Content-type" : "text/plain"})
		response.end("I'm sorry, the page you are looking for cannot be found")

	}

})

server.listen(3000)
console.log(`Server is now accessible at localhost ${port}.`)