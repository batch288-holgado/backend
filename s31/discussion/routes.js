const http = require('http')

// Creates a variable port to store the port number; this is an alternative method compared to index.js
const port = 8888

const server = http.createServer((request, response) => {
	
	if (request.url == '/greetings') {
		response.writeHead(200, {'Content-Type' : "text/plain"})

		response.end('Hellow Michael Jordan!')
	} else if (request.url == '/homepage'){
		response.writeHead(200, {'Content-Type' : 'text/plain'})

		response.end('This is the homepage')
	}	else {
		response.writeHead(404, {'Content-Type' : 'text/plain'})

		response.end('Hey! Page is not available')
	}

})

server.listen(port)
console.log(`Server is now accessible at localhost:${port}.`);