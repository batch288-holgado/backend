// Use the "require" directive to load Node/js modules
	/*
		- A module is a software component or part of a program that contains one or more routines
		
		- The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol

		- In that way, HTTP is a protocol that allows the fetching of resources such as HTML documents

		- Clients (browser) and Servers (NodeJS/ExpressJS Applications) communicate by exchanging individual messages

		- These messages are sent by the clients, usually a web browser and called "request"

		- The messages sent by the server as an answer are called "response"
	*/


let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives responses back to the client.
	/*
		- A "port" is a virtual point where network connections start and end

		- The HTTP module has a createServer() method that accepts a function as an arguement and allows for creation of a server

		- The arguements passed in the function are request and response objects (data types) containing methods that allow us to receive and request from the client and send responses back
	*/

// The server will be assigned to port 4000 via the ".listen(4000) method where the server will listen to any requests it receives, eventually allowing communcation with the server"

http.createServer(function (request, response) {
	
	/*
		.writeHead() method is used to:
			- Set a stauts code for the response - a 200 means ok
			- Sets the content-type of the response as a plain text message
	*/
	response.writeHead(200, {'Content-Type' : 'text/plain'});

	// Send the response with text content "Hello World?!"
	response.end('Hello world?!');

}).listen(8000);

// Doing this allows the console to print the message while the server is running in gitBash 
console.log('Server running at localhost:8000')

/*
	To activate the server:
		- In gitbash: type 'node fileName' to trigger the server
		- Press ctrl+c to end the program gracefully (when you will end it)
		- type 'localhost:4000' or whatever value is in listen() in the internet server (google chrome) to host the website
		- Type '$ npm install -g nodemon' (if not yet installed) in the same folder where the file is located -> type 'nodemon fileName' to activate nodemon
		- nodemon is used so that we won't have to run the server again and again (e.g. when you change the value of listen(xxxx) to another number, ctrl+s will automatically change it in gitbash)
*/ 


 

